﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LP_1_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        span{color:red;}
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="signupForm">
         <asp:HiddenField ID="clickId" runat="server" />
           
    <div>
        <asp:Literal ID="customerrors" runat="server"></asp:Literal>
        <div>
            <div>Full name:</div>
            <div><asp:TextBox runat="server" ID="fullname"></asp:TextBox></div>
            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="fullname" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please fill your fullname"></asp:RequiredFieldValidator>
        </div>
        
        <div>
            <div>Email:</div>
            <div><asp:TextBox runat="server" ID="email"></asp:TextBox></div>
            <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="email" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please fill your email"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="Dynamic" ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
        </div>
   
        <div>
            <div>Gender:</div>
            <div><asp:DropDownList ID="gender" runat="server">
                <asp:ListItem Value="M">Male</asp:ListItem>
                <asp:ListItem Value="F">Female</asp:ListItem>
                </asp:DropDownList></div>
        </div>

        <div>
            <div>Phone:</div>
            <div><asp:TextBox runat="server" ID="phone"></asp:TextBox></div>
             <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="phone" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please fill your phone"></asp:RequiredFieldValidator>
            
               <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator3" runat="server" ValidationExpression="^\d{7,20}$" ControlToValidate="phone" ErrorMessage="Invalid phone Format, please use numbers only"></asp:RegularExpressionValidator>
      
        </div>

        <div>
            <div>Birthdate:</div>
            
            <div>
                <asp:DropDownList ID="birthday" runat="server">

                </asp:DropDownList>
                <asp:DropDownList ID="birthmonth" runat="server">

                </asp:DropDownList>
                <asp:DropDownList ID="birthyear" runat="server">

                </asp:DropDownList>

            </div>
        </div>

        <div>
            <div>Street:</div>
            <div><asp:TextBox runat="server" ID="street"></asp:TextBox></div>
        </div>
         <div>
            <div>City:</div>
            <div><asp:TextBox runat="server" ID="city"></asp:TextBox></div>
        </div>
         <div>
            <div>State:</div>
            <div><asp:TextBox runat="server" ID="state"></asp:TextBox></div>
        </div>
               <div>
            <div>Country:</div>
            <div><asp:DropDownList ID="country" runat="server">

                </asp:DropDownList></div>
        </div>
              <div>
            <div>postalcode:</div>
            <div><asp:TextBox runat="server" ID="postalcode"></asp:TextBox></div>
        </div>
              <div>
           <div><asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" /></div>
        </div>
    </div>
    </asp:Panel>


    <asp:Panel runat="server" ID="thankyou">
        <div>Thank you for signing up!</div>
    </asp:Panel>
    
    
    </form>

    


</body>
</html>
