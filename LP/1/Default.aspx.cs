﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LP_1_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        signupForm.Visible = true;
        thankyou.Visible = false;
        if(!IsPostBack)
        {
            clickId.Value = Request.QueryString["transactionId"].ToString();

            int i = 0;
            for(i=1;i<=31;i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                birthday.Items.Add(item);
            }
            for (i = 1; i <= 12; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                birthmonth.Items.Add(item);
            }
            for (i = DateTime.Now.Year - 100; i <= DateTime.Now.Year; i++)
            {
                ListItem item = new ListItem(i.ToString(), i.ToString());
                birthyear.Items.Add(item);
            }
            birthyear.SelectedIndex = 80;

            string ipaddress = Request.ServerVariables["remote_addr"].ToString();
            if (ipaddress.IndexOf(":") >= 0) ipaddress = "10.0.0.1";

            string ipcountry = Countries.get(ipaddress);

            List<Country> countries = Countries.get();
            for(i=0;i<countries.Count;i++)
            {
                ListItem item = new ListItem(countries[i].countryName.ToLower(), countries[i].countryId.ToString());
                country.Items.Add(item);
                if (ipcountry == countries[i].countryName)
                    country.SelectedIndex = country.Items.Count - 1;
            }

        }
        

        
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        string mCountry = country.SelectedItem.Text;
        string mPostalcode = postalcode.Text;
        string mState = state.Text;
        string mCity = city.Text;
        string mStreet = street.Text;

        string ipaddress = Request.ServerVariables["remote_addr"].ToString();
        if(ipaddress.IndexOf(":")>=0) ipaddress="10.0.0.1";

        string mBirthday = birthday.SelectedValue;
        string mBirthmonth = birthmonth.SelectedValue;
        string mBirthyear = birthyear.SelectedValue;

        string mPhone = phone.Text;
        string mGender = gender.SelectedValue;
        string mFullname = fullname.Text;
        string mEmail = email.Text;

        customerrors.Visible = false;

        string mClickid = clickId.Value;

        string result = savelead.saveLeadToDb(mClickid, mFullname, mEmail, mPhone, mGender, mBirthday, mBirthmonth, mBirthyear, mStreet, mCity, mState, mPostalcode, mCountry, ipaddress );
        if(result.IndexOf("\"message\":\"success\"")<0)
        {
            //{"message":"error","errorMessages":{"email":["Please enter a valid email address "]}}
            StringBuilder sb = new StringBuilder();
            int i = 0;
            sb.Append("<div style=\"color:red\">");
            while(i>-1)
            {
                i = result.IndexOf("[",i+1);
                if(i>=0)
                {
                    int lastIndex = result.IndexOf("]", i);
                    sb.Append(result.Substring(i + 1, lastIndex - 1 - i).Trim('"'));
                    sb.Append("<br/>");
                }
            }
            sb.Append("</div>");
            customerrors.Text = sb.ToString();
            customerrors.Visible = true;
            sb.Clear();
        }
        else
        {
            thankyou.Visible = true;
            signupForm.Visible = false;

        }
        
    }
}