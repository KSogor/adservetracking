﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for postbacks
/// </summary>
public class postbacks
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
	public postbacks()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void save(postbacksNew postbackitem)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("PostBacksSave", con))
            {
                cmd.Parameters.AddWithValue("@postbackId", postbackitem.postbackId);
                cmd.Parameters.AddWithValue("@affiliateId", postbackitem.affId);
                cmd.Parameters.AddWithValue("@offerId", postbackitem.offerId);
                cmd.Parameters.AddWithValue("@postbackurl", postbackitem.postbackurl);
                cmd.Parameters.AddWithValue("@conversionType", postbackitem.conversionType);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    public static void loadPostBacks(List<postbacksItem> postbacks, int postbackId, int affiliateId)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("PostBacksGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@postbackId", postbackId);
                cmd.Parameters.AddWithValue("@affiliateId", affiliateId);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    postbacksItem postbackItem = new postbacksItem();
                    postbackItem.postbackId = Convert.ToInt32(reader["postbackId"].ToString());
                    postbackItem.affId = Convert.ToInt32(reader["affid"].ToString());
                    postbackItem.conversionType = Convert.ToInt32(reader["conversionType"].ToString());
                    postbackItem.conversionName = utils.getConversionByType(postbackItem.conversionType);
                    postbackItem.offerId = Convert.ToInt32(reader["offerid"].ToString());
                    postbackItem.postbackurl = reader["postbackurl"].ToString();
                    postbackItem.category = utils.getCategoryById(Convert.ToInt32(reader["category"].ToString()));
                    postbackItem.advertiser = reader["advertisername"].ToString();
                    postbackItem.landingPage = reader["lp"].ToString();
                    postbacks.Add(postbackItem);
                }
                reader.Close();
                reader = null;
            }
        }


    }

    public static postbacksNew loadPostBacks(int postbackId)
    {
        postbacksNew postbackitem = new postbacksNew();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("PostBacksGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@postbackId", postbackId);
                cmd.Parameters.AddWithValue("@affiliateId", "0");
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    postbackitem.postbackId = Convert.ToInt32(reader["postbackId"].ToString());
                    postbackitem.affId = Convert.ToInt32(reader["affid"].ToString());
                    postbackitem.conversionType = Convert.ToInt32(reader["conversionType"].ToString());
                    postbackitem.offerId = Convert.ToInt32(reader["offerid"].ToString());
                    postbackitem.postbackurl = reader["postbackurl"].ToString();
                    postbackitem.advertiserId = Convert.ToInt32(reader["advertiserid"].ToString());
                }
                reader.Close();
                reader = null;
            }
        }
        return postbackitem;
       

    }

 
}

public class postbacksNew
{
    public int postbackId { get; set; }
    public int affId { get; set; }
    public int offerId { get; set; }
    public string postbackurl { get; set; }//250 chars
    public int conversionType { get; set; }
    public int advertiserId { get; set; }


}
public class postbacksItem
{
    public int postbackId { get; set; }
    public int affId { get; set; }
    public int offerId { get; set; }
    public string postbackurl { get; set; }//250 chars
    public int conversionType { get; set; }
    public string category { get; set; }
    public string advertiser { get; set; }
    public string landingPage { get; set; }
    public string conversionName { get; set; }

}