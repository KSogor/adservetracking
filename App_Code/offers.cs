﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for offers
/// </summary>
///


    public class offers
    {
        public enum revTypes
        {
            CPA=1,
            RevShare=2,
            CPL=3
        }

        //members
        public int offerId { get; set; }
        public int advertiserId { get; set; }

        public string advertiserName { get; set; }
        public string landingPage { get; set; }

        public string staticvalues { get; set; }
        public int category { get; set; }
        public int revenueType { get; set; }
        public decimal revenueValue { get; set; }
        public int payoutType { get; set; }
        public decimal payoutValue { get; set; }
        public bool active { get; set; }
        public string offername { get; set; }


        public offers()
        {
         
        }


        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;



        // Save affiliate to DB, if affiliateid=0, add new affiliate, else updates existing affiliate
        public static void SaveOffer(offers offer)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("OfferSave", con))
                {
                    cmd.Parameters.AddWithValue("@offerID", offer.offerId);
                    cmd.Parameters.AddWithValue("@advertiserID", offer.advertiserId);
                    cmd.Parameters.AddWithValue("@lp", offer.landingPage);
                    cmd.Parameters.AddWithValue("@category", offer.category);
                    cmd.Parameters.AddWithValue("@revenuetype", offer.revenueType);
                    cmd.Parameters.AddWithValue("@revenuevalue", offer.revenueValue);
                    cmd.Parameters.AddWithValue("@payouttype", offer.payoutType);
                    cmd.Parameters.AddWithValue("@payoutvalue", offer.payoutValue);
                    cmd.Parameters.AddWithValue("@active", offer.active);
                    cmd.Parameters.AddWithValue("@staticvalues", offer.staticvalues);
                    cmd.Parameters.AddWithValue("@offername", offer.offername);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }

        // retreive all affiliates or 1 affiliate, depends if affiliate id=0 or not
        public static List<offers> getOffer(int offerId)
        {
            List<offers> Offers = new List<offers>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("OffersGet", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@offerID", offerId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        offers off = new offers();
                        off.offerId = Convert.ToInt32(reader["offerID"].ToString());
                        off.advertiserId = Convert.ToInt32(reader["advertiserID"].ToString());
                        off.landingPage = reader["lp"].ToString();
                        off.category = Convert.ToInt32(reader["category"].ToString());
                        off.revenueType = Convert.ToInt32(reader["revenuetype"].ToString());
                        off.revenueValue = Convert.ToDecimal(reader["revenuevalue"].ToString());
                        off.payoutType = Convert.ToInt32(reader["payouttype"].ToString());
                        off.payoutValue = Convert.ToDecimal(reader["payoutvalue"].ToString());
                        off.active = Convert.ToBoolean(reader["active"].ToString());
                        off.staticvalues = reader["staticvalues"].ToString();
                        off.offername = reader["offername"].ToString();
                        off.advertiserName = reader["advertisername"].ToString();

                        Offers.Add(off);
                    }
                    reader.Close();
                    reader = null;
                    return Offers;
                }
            }
        }

        public static List<offers> getOfferByAdvertiser(int advertiserId)
        {
            List<offers> Offers = new List<offers>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("OffersGetByAdvertiser", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@advertiserId", advertiserId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        offers off = new offers();
                        off.offerId = Convert.ToInt32(reader["offerID"].ToString());
                        off.advertiserId = Convert.ToInt32(reader["advertiserID"].ToString());
                        off.landingPage = reader["lp"].ToString();
                        off.category = Convert.ToInt32(reader["category"].ToString());
                        off.revenueType = Convert.ToInt32(reader["revenuetype"].ToString());
                        off.revenueValue = Convert.ToDecimal(reader["revenuevalue"].ToString());
                        off.payoutType = Convert.ToInt32(reader["payouttype"].ToString());
                        off.payoutValue = Convert.ToDecimal(reader["payoutvalue"].ToString());
                        off.active = Convert.ToBoolean(reader["active"].ToString());
                        off.staticvalues = reader["staticvalues"].ToString();
                        off.offername = reader["offername"].ToString();
                        off.advertiserName = reader["advertisername"].ToString();

                        Offers.Add(off);
                    }
                    reader.Close();
                    reader = null;
                    return Offers;
                }
            }
        }


        public static List<offers> getOfferByAdvertiser(string advertisers)
        {
            List<offers> Offers = new List<offers>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("OffersGetByAdvertiserIds", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@advertisersIds", advertisers);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        offers off = new offers();
                        off.offerId = Convert.ToInt32(reader["offerID"].ToString());
                        off.advertiserId = Convert.ToInt32(reader["advertiserID"].ToString());
                        off.landingPage = reader["lp"].ToString();
                        off.category = Convert.ToInt32(reader["category"].ToString());
                        off.revenueType = Convert.ToInt32(reader["revenuetype"].ToString());
                        off.revenueValue = Convert.ToDecimal(reader["revenuevalue"].ToString());
                        off.payoutType = Convert.ToInt32(reader["payouttype"].ToString());
                        off.payoutValue = Convert.ToDecimal(reader["payoutvalue"].ToString());
                        off.active = Convert.ToBoolean(reader["active"].ToString());
                        off.staticvalues = reader["staticvalues"].ToString();
                        off.offername = reader["offername"].ToString();
                        off.advertiserName = reader["advertisername"].ToString();

                        Offers.Add(off);
                    }
                    reader.Close();
                    reader = null;
                    return Offers;
                }
            }
        }

    }
