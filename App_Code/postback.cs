﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for postbacks
/// </summary>
public class postbackCS
{
	public static string[] keys = { "{clickid}", "{tag}", "{ipaddress}", "{params}", "{campaign}", "{traffic}", "{keyword}", "{affid}", "{googleaid}", "{iosifa}", "{deposit_amount}", "{fd}" };

	static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

    public static void postbackSend(string clickId,int conversionType)
	{
        List<PostBackItem> postBacks = load(clickId, conversionType);
        send(postBacks);
    }

    public static void send(List<PostBackItem> postBacks)
    {
        for(int i=0;i<postBacks.Count;i++)
        {
            string url = postBacks[i].postbackUrl;

			string[] optionalValues = { postBacks[i].clickid, postBacks[i].tag, postBacks[i].ipaddress, postBacks[i].transactionId, postBacks[i].campaign, postBacks[i].traffic, postBacks[i].keyword, postBacks[i].affid, postBacks[i].googleaid, postBacks[i].iosifa, postBacks[i].deposit_amount, postBacks[i].fd };

			for (int p = 0; p < keys.Length; p++)
			{
				url = url.Replace(keys[p], optionalValues[p]);
			}

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = WebRequestMethods.Http.Post;
            try
            {
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                 using (StreamReader sr = new StreamReader(stream))
                 {
                    stream.Flush();
                    string resultString = sr.ReadToEnd();
                    logwriter.write(url, "postback.txt");
                    logwriter.write(resultString, "postback.txt");
                    logwriter.write("--------------------", "postback.txt");
                 }

            }
            catch(Exception exc)
            {
                logwriter.write(DateTime.Now.ToString(), "postback-error.txt");
                logwriter.write(exc.InnerException.ToString(), "postback-error.txt");
                logwriter.write(exc.Message.ToString(), "postback-error.txt");
                logwriter.write("--------------------", "postback-error.txt");
            }
        }
    }

    public static List<PostBackItem> load(string clickId, int conversionType)
    {
        List<PostBackItem> postbacks = new List<PostBackItem>();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("PostBackGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@clickId", clickId);
                cmd.Parameters.AddWithValue("@conversionType", conversionType);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					PostBackItem item = new PostBackItem()
					{
						clickid = clickId,
						postbackUrl = reader["postbackurl"].ToString(),
						transactionId = reader["params"].ToString(),
						googleaid = reader["googleaid"].ToString(),
						iosifa = reader["iosifa"].ToString(),
						deposit_amount = reader["deposit_amount"].ToString(),
						fd = reader["fd"].ToString(),
						
						tag = reader["TagId"].ToString(),
						ipaddress = reader["ipaddress"].ToString(),
						campaign = reader["campaign"].ToString(),
						traffic = reader["trafficsource"].ToString(),
						keyword = reader["keyword"].ToString(),
						affid = reader["affid"].ToString()
					};
                    postbacks.Add(item);
                }
                reader.Close();
                reader = null;
            }
        }
        return postbacks;
    }
}
public struct PostBackItem
{
    public string transactionId { get; set; }
    public string postbackUrl { get; set; }
	public string googleaid { get; set; }
	public string iosifa { get; set; }
	public string deposit_amount { get; set; }
	public string fd { get; set; }

	public string clickid { get; set; }
	public string tag { get; set; }
	public string ipaddress { get; set; }
	public string campaign { get; set; }
	public string traffic { get; set; }
	public string keyword { get; set; }
	public string affid { get; set; }

}