﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for campaignes
/// </summary>
///


    public class Campaign
    {
        //members
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignDescription { get; set; }
        public bool CampaignIsDefault { get; set; }

        //public List<CampaignRule> Rules { get; set; }

        public string RulesCount { get; set; }

        public Campaign()
        {
        }

        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

        // Save affiliate to DB, if affiliateid=0, add new affiliate, else updates existing affiliate
        public static void SaveCampaign(Campaign campaign)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Campaign_Save", con))
                {
                    cmd.Parameters.AddWithValue("@CampaignId", campaign.CampaignId);
                    cmd.Parameters.AddWithValue("@CampaignName", campaign.CampaignName);
                    cmd.Parameters.AddWithValue("@CampaignDescription", campaign.CampaignDescription);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }

        // retreive all affiliates or 1 affiliate, depends if affiliate id=0 or not
        public static Campaign getCampaign(int campaignId)
        {
           var campaign = new Campaign();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Campaign_Get", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@CampaignID", campaignId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        campaign.CampaignId = Convert.ToInt32(reader["CampaignId"].ToString());
                        campaign.CampaignName = reader["CampaignName"].ToString();
                        campaign.CampaignDescription = reader["CampaignDescription"].ToString();
                        campaign.CampaignIsDefault = (bool)reader["CampaignIsDefault"];

                        campaign.RulesCount = reader["Rules_count"] == DBNull.Value ? "0" : reader["Rules_count"].ToString();

                    }
                    reader.Close();
                    reader = null;
                    return campaign;
                }
            }
        }

        public static Campaign GetDefault() {
            var campaign = new Campaign();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Campaign_GetDefault", con))
                {
                    SqlDataReader reader;
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        campaign.CampaignId = Convert.ToInt32(reader["CampaignId"].ToString());
                        campaign.CampaignName = reader["CampaignName"].ToString();
                        campaign.CampaignDescription = reader["CampaignDescription"].ToString();
                        campaign.CampaignIsDefault = (bool)reader["CampaignIsDefault"];

                        campaign.RulesCount = reader["Rules_count"] == DBNull.Value ? "0" : reader["Rules_count"].ToString();

                    }
                    reader.Close();
                    reader = null;
                    return campaign;
                }
            }
        }

        public static List<Campaign> getAll()
        {
            var campaigns = new List<Campaign>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Campaign_Get", con))
                {
                    SqlDataReader reader;
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var campaign = new Campaign();
                        campaign.CampaignId = Convert.ToInt32(reader["CampaignId"].ToString());
                        campaign.CampaignName = reader["CampaignName"].ToString();
                        campaign.CampaignDescription = reader["CampaignDescription"].ToString();
                        campaign.CampaignIsDefault = (bool)reader["CampaignIsDefault"];
                        campaign.RulesCount = reader["Rules_count"] == DBNull.Value ? "0" : reader["Rules_count"].ToString();
                        campaigns.Add(campaign);
                    }
                    reader.Close();
                    reader = null;
                    return campaigns;
                }
            }
        }
    }
