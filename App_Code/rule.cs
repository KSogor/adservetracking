﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for campaign rules
/// </summary>
///


    public class CampaignRule
    {
        //members
        public int RuleId { get; set; }
        public string RuleDescription { get; set; }
        public string GeoTag { get; set; }
        public Dictionary<int, int> Offers { get; set; }
        public string OffersString { get; set; }
        public string OffersInDbFormat { get; set; }
        public bool RuleIsDefault { get; set; }

        public CampaignRule()
        {
         
        }


        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
        
        // retreive all affiliates or 1 affiliate, depends if affiliate id=0 or not
        public static CampaignRule getRule(int campaignId, int ruleId)
        {
            var rule = new CampaignRule();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rule_Get", con))
                {
                    SqlDataReader reader;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CampaignID", campaignId);
                    cmd.Parameters.AddWithValue("@RuleID", ruleId);
                    con.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        
                        rule.RuleId = Convert.ToInt32(reader["RuleId"].ToString());
                        rule.RuleDescription = reader["RuleDescription"].ToString();
                        rule.GeoTag = reader["GeoTag"].ToString();
                        rule.Offers = ParseOfferIdPercentage(reader["Offers"].ToString());
                        rule.OffersString = FormOffersString(rule.Offers);
                        rule.RuleIsDefault = (bool)reader["RuleIsDefault"];

                        
                    }

                    reader.Close();
                    reader = null;
                    return rule;
                }
            }
        }

        public static List<CampaignRule> getAll(int campaignId)
        {
            var rules = new List<CampaignRule>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rule_Get", con))
                {
                    SqlDataReader reader;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CampaignID", campaignId);
                    con.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var rule = new CampaignRule();
                        rule.RuleId = Convert.ToInt32(reader["RuleId"].ToString());
                        rule.RuleDescription = reader["RuleDescription"].ToString();
                        rule.GeoTag = reader["GeoTag"].ToString();
                        rule.OffersInDbFormat = reader["Offers"].ToString();
                        rule.Offers = ParseOfferIdPercentage(reader["Offers"].ToString());
                        rule.OffersString = FormOffersString(rule.Offers);
                        rule.RuleIsDefault = (bool)reader["RuleIsDefault"];
                        rules.Add(rule);
                    }

                    reader.Close();
                    reader = null;
                    return rules;
                }
            }
        }

        public static void SaveCampaignRule(int campaignId, CampaignRule rule)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rule_Save", con))
                {
                    cmd.Parameters.AddWithValue("@CampaignId", campaignId);
                    cmd.Parameters.AddWithValue("@RuleId", rule.RuleId);
                    cmd.Parameters.AddWithValue("@RuleDescription", rule.RuleDescription);
                    cmd.Parameters.AddWithValue("@GeoTag", rule.GeoTag);
                    cmd.Parameters.AddWithValue("@Offers", rule.OffersInDbFormat);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public static void DeleteCampaignRule(int campaignId, int ruleId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rule_Delete", con))
                {
                    cmd.Parameters.AddWithValue("@CampaignId", campaignId);
                    cmd.Parameters.AddWithValue("@RuleId", ruleId);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }



        private static Dictionary<int, int> ParseOfferIdPercentage(string generalLine) 
        {
            var res = new Dictionary<int, int>();
            if (!string.IsNullOrEmpty(generalLine))
            {
                foreach (var pair in generalLine.Split(';'))
                {
                    var offerId = Convert.ToInt32(pair.Split('-')[0]);
                    var percentage = Convert.ToInt32(pair.Split('-')[1]);
                    if (!res.Keys.Contains(offerId))
                    {
                        res.Add(offerId, percentage);
                    }
                }
            }

            return res;
        }

        private static string FormOffersString(Dictionary<int, int> offersSet)
        {
            var res = new List<string>();

            foreach (var pair in offersSet)
            {
                res.Add(string.Format("{0}({1}%)",pair.Key, pair.Value));
            }

            return string.Join(", ", res);
        }
        
        
       
       

    }
