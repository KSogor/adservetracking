﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for countries
/// </summary>
public class Countries
{
	public Countries()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

    public static string get(string ipaddress)
    {
        string country="not found";
        int[] parsedIp = ipaddress.Split('.').Select(h=>Int32.Parse(h)).ToArray();
        int ip = parsedIp[0] * 256 * 256 * 256 + parsedIp[1] * 256 * 256 + parsedIp[2] * 256 + parsedIp[3];

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("CountryGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@calculatedIP", ip);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    country = reader["country_name"].ToString();
                }
                reader = null;
            }
        }
        return country;
    }

    public static List<Country> get()
    {
        List<Country> countries = new List<Country>();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("CountriesGet", con))
            {
                SqlDataReader reader;
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Country item = new Country();
                    item.countryId=Convert.ToInt32(reader["countryid"]);
                    item.countryName=reader["countryname"].ToString();
                    countries.Add(item);
                }
                reader = null;
            }
        }
        return countries;
    }

    public static List<Country> getCountryCodes()
    {
        List<Country> countryCodes = new List<Country>();
        countryCodes.Add(new Country() { countryName = "ALL", countryCode = "ALL"});
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("CountryCodes_Get", con))
            {
                SqlDataReader reader;
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Country country = new Country();
                    country.countryCode = reader["country_code"].ToString();
                    country.countryName = reader["country_name"].ToString();
                    countryCodes.Add(country);
                }
                reader = null;
            }
        }

        return countryCodes;
    }


}
public class Country
{
    public int countryId { get; set; }
    public string countryName { get; set; }
    public string countryCode { get; set; }
}