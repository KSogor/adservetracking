﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for permissions
/// </summary>
public class permissions
{
    Dictionary<string, bool> permissionsSet = new Dictionary<string, bool>(); 
   	public static bool hasPermission(string page)
	{
        if (HttpContext.Current.Session["affiliate"] != null)
        {
            Affiliates aff = (Affiliates)HttpContext.Current.Session["affiliate"];
            if (aff.affiliateType == 1)
                return true;
        }
        else if (HttpContext.Current.Session["advertiser"] != null)
        {
            advertisers adv = (advertisers)HttpContext.Current.Session["advertiser"];
            return false;
        }
        return false;
	}

    public static bool isAffiliate()
    {
        if (HttpContext.Current.Session["affiliate"] != null)
        {
            Affiliates aff = (Affiliates)HttpContext.Current.Session["affiliate"];
            if (aff.affiliateType == 1)
                return false;
            else
                return true;
        }
        return false;
    }

}