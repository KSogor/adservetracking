﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for advertisers
/// </summary>
public class advertisers
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

    public string name { get; set; }
    public string url { get; set; }
    public string email { get; set; }
    public string advertiserPostURL { get; set; }
    public string advertiserPostBackUrl { get; set; }
    public string advertiserpassword { get; set; }
    public int id { get; set; }

	public advertisers()
    {


	}


    public static bool checkLogin(string username, string password)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AdvertiserLogin", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@email", username);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                if (!reader.IsClosed)
                {
                    while (reader.Read())
                    {
                        advertisers adv = new advertisers();
                        adv.email = reader["advertiseremail"].ToString();
                        adv.id = Convert.ToInt32(reader["advertiserid"].ToString());
                        adv.name = reader["advertisername"].ToString();
                        HttpContext.Current.Session.Add("advertiser", adv);
                        HttpContext.Current.Session.Add("permissionAdv", adv.id);
                        reader = null;
                        return true;
                    }

                }
                reader = null;
                return false;
            }
        }
    }

    // Save advertisers to DB, if advertisers=0, add new advertisers, else updates existing advertisers
    public void saveAdvertiser(advertisers advertiser)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AdvertiserSave", con))
            {
                cmd.Parameters.AddWithValue("@advertiserid", advertiser.id);
                cmd.Parameters.AddWithValue("@advertisername", advertiser.name);
                cmd.Parameters.AddWithValue("@advertiseremail", advertiser.email);
                cmd.Parameters.AddWithValue("@advertiserURL", advertiser.url);
                cmd.Parameters.AddWithValue("@PostURL", advertiser.advertiserPostURL);
                cmd.Parameters.AddWithValue("@PostBackURL", advertiser.advertiserPostBackUrl);
                cmd.Parameters.AddWithValue("@AdvertiserPassword", advertiser.advertiserpassword);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
    // retreive all advertisers or 1 advertisers, depends if advertisers id=0 or not
    public static List<advertisers> getAdvertiser(int advertiserID)
    {
        List<advertisers> Advertisers = new List<advertisers>();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AdvertisersGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@advertiserid", advertiserID);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    advertisers adv = new advertisers();
                    adv.id = Convert.ToInt32(reader["advertiserid"].ToString());
                    adv.name = reader["advertisername"].ToString();
                    adv.url = reader["advertiserURL"].ToString();
                    adv.email = reader["advertiseremail"].ToString();
                    adv.advertiserPostURL = reader["advertiserPostURL"].ToString();
                    adv.advertiserPostBackUrl = reader["advertiserPostBackUrl"].ToString();
                    adv.advertiserpassword = reader["advertiserpassword"].ToString();
                    Advertisers.Add(adv);
                }
                reader = null;
                return Advertisers;
            }
        }
    }
    
}