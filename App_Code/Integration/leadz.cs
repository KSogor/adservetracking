﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for leadz
/// </summary>
public class leadz
{
	public leadz()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string getFromQuery(string key)
    {

        if (HttpContext.Current.Request.Form[key] != null)
            return HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Form[key].ToString());
        else
            return "";
    }

   
    public string sendLead(string formId,string clickId)
    {
        string apiKey = HttpContext.Current.Server.UrlEncode("jei%39dEsk25P9mc@8&6jd47Edi");
        string partnerId = HttpContext.Current.Server.UrlEncode("275678");
        string form = formId;

        string[] keysQuery = { "leaddate", "userip", "email", "fullname", "gender", "phone", "birthday", "birthmonth", "birthyear", "street", "city", "state", "country", "postalcode" };
        string[] keys = {"leadCreated", "userIp", "email", "fullName", "Gender", "phone", "birthDate_day", "birthDate_month", "birthDate_year", "street", "city", "state", "country", "postalCode"};

        using (var client = new WebClient())
        {
            NameValueCollection values = new NameValueCollection();
            values["apiKey"] = apiKey;
            values["partnerId"] = partnerId;
            values["form"] = formId;
            values["clickid"] = clickId;
            for(int i=0;i<keys.Length;i++)
            {
                values[keys[i]] = getFromQuery(keysQuery[i]);
            }
            
            byte[] response = client.UploadValues("https://leadz.com/api.php?api=SubmitLead", values);
            string responseString = Encoding.Default.GetString(response);
            return responseString;
        }
    }
}