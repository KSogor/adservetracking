﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for utils
/// </summary>
public class utils
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    
    public static string[] conversionTypes = {"Please select","Sign up","Conversion","Deposit"};
	public utils()
	{
    }
    public static bool isManager()
    {
        Affiliates aff = (Affiliates)HttpContext.Current.Session["affiliate"];
        if (aff == null) return false;
        return (aff.affiliateType == 1);

    }

	public static string cleanForJSON(string s)
         {
        if (s == null || s.Length == 0) {
            return "";
        }

        char         c = '\0';
        int          i;
        int          len = s.Length;
        StringBuilder sb = new StringBuilder(len + 4);
        String       t;

        for (i = 0; i < len; i += 1) {
            c = s[i];
            switch (c) {
                case '\\':
                case '"':
                    sb.Append('\\');
                    sb.Append(c);
                    break;
                case '/':
                    sb.Append('\\');
                    sb.Append(c);
                    break;
                case '\b':
                    sb.Append("\\b");
                    break;
                case '\t':
                    sb.Append("\\t");
                    break;
                case '\n':
                    sb.Append("\\n");
                    break;
                case '\f':
                    sb.Append("\\f");
                    break;
                case '\r':
                    sb.Append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        t = "000" + String.Format("X", c);
                        sb.Append("\\u" + t.Substring(t.Length - 4));
                    } else {
                        sb.Append(c);
                    }
                    break;
            }
        }
        return sb.ToString();
    }

    public static List<Deals> getDeals()
    {
        return (List<Deals>)HttpContext.Current.Application.Get("deals");

    }

    public static List<Categories> getCategories()
    {
        return (List<Categories>)HttpContext.Current.Application.Get("categories");
    }
    public static string getCategoryById(int categoryId)
    {
        List<Categories> cats = (List<Categories>)HttpContext.Current.Application.Get("categories");
        string category = "";
        category = cats.Find(x => x.categoryId == categoryId).ToString();
        return category;
    }

    public static string getConversionByType(int typeId)
    {
        if (typeId > 0 && typeId < conversionTypes.Length)
            return conversionTypes[typeId];
        else
            return "";
    }

    public static List<string> getReportParameterValues(string parameterName)
    {
        var res= new List<string>();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("ReportGetParameters", con))
            {
                SqlDataReader reader;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@parameterName", parameterName);
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(reader["Value"].ToString());
                }
                reader = null;
            }
        }

        return res;
    }
}