﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections;
using System.Web;

/// <summary>
/// Summary description for tags
/// </summary>
public class tags
{

    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public int affiliateId { get; set; }
    public int tagid { get; set; }
    public string tag { get; set; }
    public tags()
	{
	}

    // looking for a tag and retreiving its alias from a cached table
    // if not found, calling a save function to store it in the database.
    public static string Find(Dictionary<int, Hashtable> TagsTable, int affiliateId, string TagName)
    {
        string newTagId;
        // if affiliate exist in the tag table ( has at least 1 tag already ) 
        if(TagsTable.ContainsKey(affiliateId))
        {
            // if tag exist already, fetching it
            Hashtable affTags = TagsTable[affiliateId];
            if (affTags.ContainsKey(TagName))
                return affTags[TagName].ToString();
            else  //if tag doesn't exist, adding it both to the database and to the cached table
            {
                newTagId = Save(affiliateId,TagName);
                affTags.Add(TagName,newTagId);
                TagsTable[affiliateId] = affTags;
                return newTagId;
            }
        }
        // if affiliate doesn't exist in the cache table, adding it and creating a new tag for it
        Hashtable newTags = new Hashtable();
        TagsTable.Add(affiliateId, newTags);
        newTagId = Save(affiliateId,TagName);
        newTags.Add(TagName, newTagId);
        return newTagId;
    }
    // Saving a new tag in the database and returning the key
    public static string Save(int affiliateId, string TagName)
    {
        string TagId = "";
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("TagsSave", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@affid", affiliateId);
                cmd.Parameters.AddWithValue("@tagname", TagName);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                     TagId = reader["tagid"].ToString();
                }
                reader.Close();
                reader = null;
            }
        }
        return TagId;
    }
    // loading all tags from the database, storing them in a key (affiliate id) value ( old tag, new tag ) 
    public static void Load(Dictionary<int, Hashtable> TagsTable)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("TagsGet", con))
            {
                SqlDataReader reader;
//                cmd.Parameters.AddWithValue("@ipaddress", ipaddress);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                int currentAffiliate = 0;
                Hashtable affiliateTags = new Hashtable();
                while (reader.Read())
                {
                    int newAffiliateId = Convert.ToInt32(reader["affiliateid"].ToString());
                    if(currentAffiliate==0 || currentAffiliate!=newAffiliateId)
                    {
                        if (currentAffiliate != 0)
                        {
                            TagsTable.Add(currentAffiliate, affiliateTags);
                            currentAffiliate = newAffiliateId;
                        }
                        affiliateTags = new Hashtable();
                    }
                    affiliateTags.Add(reader["tagName"].ToString(), reader["tagId"].ToString());
                }
                TagsTable.Add(currentAffiliate, affiliateTags);
                reader.Close();
                reader = null;
            }
        }
    }
}