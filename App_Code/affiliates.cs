﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for sqlquery
/// </summary>
public class Affiliates
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public Affiliates()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //#region members
    public string affiliateName { get; set; }
    public string affiliateEmail { get; set; }
    public int affiliateType { get; set; }
    public int affiliateId { get; set; }
    public string affiliateCountry { get; set; }
    public string affiliatePassword { get; set; }
    //#endregion

    // login query, returns bool true/false if user exist and sets session
    public static bool checkLogin(string username, string password)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AffiliateLogin", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@email",username);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                if (!reader.IsClosed)
                {
                    while (reader.Read())
                    {
                        Affiliates aff = new Affiliates();
                        aff.affiliateEmail = reader["affiliateemail"].ToString();
                        aff.affiliateId = Convert.ToInt32(reader["affiliateid"].ToString());
                        aff.affiliateName = reader["affiliatename"].ToString();
                        aff.affiliateCountry = reader["country"].ToString();
                        aff.affiliatePassword = reader["affiliatepassword"].ToString();
                        aff.affiliateType = Convert.ToInt32(reader["affiliatetype"].ToString());
                        HttpContext.Current.Session.Add("affiliate", aff);
                        HttpContext.Current.Session.Add("permission", aff.affiliateId);
                        reader = null;
                        return true;
                    }
                   
                }
                reader = null;
                return false; 
            }
        }
    }

    // Save affiliate to DB, if affiliateid=0, add new affiliate, else updates existing affiliate
    public static void SaveAffiliate(Affiliates affiliate)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AffiliateSave", con))
            {
                cmd.Parameters.AddWithValue("@affiliateID", affiliate.affiliateId);
                cmd.Parameters.AddWithValue("@email", affiliate.affiliateEmail);
                cmd.Parameters.AddWithValue("@affiliatename", affiliate.affiliateName);
                cmd.Parameters.AddWithValue("@password", affiliate.affiliatePassword);
                cmd.Parameters.AddWithValue("@affiliatetype", affiliate.affiliateType);
                cmd.Parameters.AddWithValue("@country", affiliate.affiliateCountry);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    // retreive all affiliates or 1 affiliate, depends if affiliate id=0 or not
    public static List<Affiliates> getAffiliate(int affiliateID)
    {
        List<Affiliates> affiliates = new List<Affiliates>();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("AffiliatesGet", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@affid", affiliateID);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Affiliates aff = new Affiliates();
                    aff.affiliateEmail = reader["affiliateemail"].ToString();
                    aff.affiliateId = Convert.ToInt32(reader["affiliateid"].ToString());
                    aff.affiliateName = reader["affiliatename"].ToString();
                    aff.affiliateCountry = reader["country"].ToString();
                    aff.affiliatePassword = reader["affiliatepassword"].ToString();
                    aff.affiliateType = Convert.ToInt32(reader["affiliatetype"].ToString());
                    affiliates.Add(aff);    
                }
                reader = null;
                return affiliates;
            }
        }
    }
}