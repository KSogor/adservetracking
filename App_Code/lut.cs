﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for lut
/// </summary>
public class lut
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public List<Categories> categories { get; set; }
    public List<Deals> deals { get; set; }
    public lut()
	{
		//
		// TODO: Add constructor logic here
		//
        categories = new List<Categories>();
        deals = new List<Deals>();

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("LutGet", con))
            {
                SqlDataReader reader;
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Categories cat = new Categories();
                    cat.categoryName = reader["categoryName"].ToString();
                    cat.categoryId = Convert.ToInt32(reader["categoryID"].ToString());
                    categories.Add(cat);
                }
                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        Deals deal = new Deals();
                        deal.dealDescription = reader["DealDescription"].ToString();
                        deal.dealName = reader["DealName"].ToString();
                        deal.dealId = Convert.ToInt32(reader["DealID"].ToString());
                        deals.Add(deal);
                    }
                }
                reader = null;
            }
        }
	}
}

public class Categories
{
    public int categoryId{get;set;}
    public string categoryName{get;set;}
}

public class Deals
{
    public int dealId { get; set; }
    public string dealName { get; set; }
    public string dealDescription { get; set; }
}