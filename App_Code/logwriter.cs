﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for logwriter
/// </summary>
public class logwriter
{
	public logwriter()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    const string logpath = @"E:\Logs\adservetracking.com\";

    public static void write(string message,string filename)
    {
        System.IO.StreamWriter file = new System.IO.StreamWriter(logpath + filename,true);
        file.WriteLine(message);
        file.Close();
    }

    public static void write(Exception message, string filename)
    {
        System.IO.StreamWriter file = new System.IO.StreamWriter(logpath + filename, true);
        file.WriteLine(message.HResult);
        file.WriteLine(message.InnerException);
        file.WriteLine(message.Message);
        file.WriteLine(message.Source);
        file.WriteLine(message.StackTrace);
        file.WriteLine(DateTime.Now.ToString());
        file.WriteLine("---------------------------------------------------");
        file.Close();
    }
}