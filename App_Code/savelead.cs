﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for savelead
/// </summary>
public class savelead
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
	public savelead()
    {

    }
    public static string saveLeadToDb(string clickid, string fullname,string email,string phone,string gender, string birthday,string birthmonth,string birthyear, string street, string city, string state,string postalcode, string country, string ipaddress)
	{
	    int offerId = 0;
        string clickId = "0";
        string ApiName="";
        string formId = "";
        string postbackresult = "";

        string[] keysQuery = { "leaddate", "userip", "email", "fullname", "gender", "phone", "birthday", "birthmonth", "birthyear", "street", "city", "state", "country", "postalcode" };
        string[] keyvalues = { DateTime.Now.ToString(), ipaddress, email, fullname, gender, phone, birthday, birthmonth, birthyear, street, city, state, country, postalcode };

        for (int i = 0; i < keysQuery.Length; i++)
            HttpContext.Current.Session.Add(keysQuery[i], keyvalues[i]);
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("ApiLoad", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@clickid", clickid);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ApiName = reader["ApiName"].ToString();
                    formId = reader["staticvalues"].ToString();
                }

            }
        }

        switch (ApiName)
        {
            case "leadz":
                leadz leadApi = new leadz();
                postbackresult = leadApi.sendLead(formId, clickId);
                break;
        }
    
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("LeadSave", con))
                {
                    cmd.Parameters.AddWithValue("@offerId", offerId);
                    cmd.Parameters.AddWithValue("@fullname", fullname);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@gender", gender);
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@birthday", birthday);
                    cmd.Parameters.AddWithValue("@birthmonth", birthmonth);
                    cmd.Parameters.AddWithValue("@birthyear", birthyear);
                    cmd.Parameters.AddWithValue("@street", street);
                    cmd.Parameters.AddWithValue("@city", city);
                    cmd.Parameters.AddWithValue("@state", state);
                    cmd.Parameters.AddWithValue("@country", country);
                    cmd.Parameters.AddWithValue("@postalcode", postalcode);
                    cmd.Parameters.AddWithValue("@conversiontype", 1);
                    cmd.Parameters.AddWithValue("@clickid", clickid);
                    cmd.Parameters.AddWithValue("@postresult", postbackresult);
                    cmd.Parameters.AddWithValue("@ipaddress", ipaddress);
                    cmd.CommandType = CommandType.StoredProcedure;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }

            return postbackresult;
    }
}