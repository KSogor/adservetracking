﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for conversions
/// </summary>
public class conversions
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
	public conversions()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void updateConversions(string clickId, int conversion, string googleaid, string iosifa, double? deposit_amount, bool? fd, string fullUrl)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("ConversionSave", con))
            {
                cmd.Parameters.AddWithValue("@clickId", clickId);
                cmd.Parameters.AddWithValue("@conversionType", conversion);
				cmd.Parameters.AddWithValue("@googleaid", googleaid);
				cmd.Parameters.AddWithValue("@iosifa", iosifa);
				cmd.Parameters.AddWithValue("@deposit_amount", deposit_amount);
				cmd.Parameters.AddWithValue("@fd", fd);
                cmd.Parameters.AddWithValue("@LastPostbackUrl", fullUrl);
				cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}