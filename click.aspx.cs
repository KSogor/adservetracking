﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class click : System.Web.UI.Page
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public string[] keys = { "{clickid}", "{tag}", "{ipaddress}","{params}","{campaign}","{traffic}","{keyword}","{affid}","{googleaid}", "{iosifa}", "{deposit_amount}", "{fd}" };
    private string getFromQuery(string key)
    {

        if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    private int getIntFromQuery(string key)
    {
        if (Request.QueryString[key] != null)
            return Convert.ToInt32(HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString()));
        else
            return 0;
    }

	private double? getDecimalFromQuery(string key)
	{
		if (!string.IsNullOrEmpty(Request.QueryString[key]))
		{
			var paramValue = Request.QueryString[key].ToString();
			return double.Parse(paramValue);
		}
		else
			return null;
	}

	private bool? getBoolFromQuery(string key)
	{
		if (!string.IsNullOrEmpty(Request.QueryString[key]))
		{
			var paramValue = Request.QueryString[key].ToString();
			if (paramValue.Length == 1) { if (paramValue == "1") return true; else return false; }
			return Convert.ToBoolean(paramValue);
		}
		else
			return null;
	}
    protected void Page_Load(object sender, EventArgs e)
    {
        string ipaddress = Request.ServerVariables["remote_addr"].ToString();
        int[] parsedIp = ipaddress.Split('.').Select(h=>Int32.Parse(h)).ToArray();
        int ip = parsedIp[0] * 256 * 256 * 256 + parsedIp[1] * 256 * 256 + parsedIp[2] * 256 + parsedIp[3];
        Response.Write(ip);

        int affid = 0;
        int.TryParse(getFromQuery("aff"), out affid);

        affid = (affid == null) ? 0 : affid;
        string tagId = getFromQuery("tag");

        int offerId = getIntFromQuery("offer");
        string p = getFromQuery("params");//Was extrenal Clickid
        string keyword = getFromQuery("keyword");
        string traffic = getFromQuery("traffic");
        string campaign = getFromQuery("campaign");
        string creative = getFromQuery("creative");
		string googleaid = getFromQuery("googleaid");
		string iosifa = getFromQuery("iosifa");
		double? deposit_amount = getDecimalFromQuery("deposit_amount");
		bool? fd = getBoolFromQuery("fd");

		string referer = (Request.ServerVariables["HTTP_REFERER"] != null) ? Request.ServerVariables["HTTP_REFERER"].ToString():"";
        string landingPage = "";
        string clickId = "";
        string newTag = "0";

        if (tagId.Length > 50) tagId.Substring(0, 49);

        if(tagId!="")
        {
            lock(Application)
            { 
                Dictionary<int, Hashtable> TagsTable = (Dictionary<int, Hashtable>)Application["Tags"];
                newTag = tags.Find(TagsTable, affid, tagId);
                Application["Tags"] = TagsTable;
            }
        }

        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("ClickSave", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@ipaddress", ipaddress);
                cmd.Parameters.AddWithValue("@affid", affid);
                cmd.Parameters.AddWithValue("@offerid", offerId);
                cmd.Parameters.AddWithValue("@params", p);
                cmd.Parameters.AddWithValue("@referer", referer);
                cmd.Parameters.AddWithValue("@tagid", Convert.ToInt32(newTag));
                cmd.Parameters.AddWithValue("@creative", creative);
                cmd.Parameters.AddWithValue("@keyword", keyword);
                cmd.Parameters.AddWithValue("@trafficsource", traffic);
                cmd.Parameters.AddWithValue("@campaign", campaign);
				cmd.Parameters.AddWithValue("@googleaid", googleaid);
				cmd.Parameters.AddWithValue("@iosifa", iosifa);
				cmd.Parameters.AddWithValue("@deposit_amount", deposit_amount);
				cmd.Parameters.AddWithValue("@fd", fd);
                cmd.Parameters.AddWithValue("@FullClickUrl", Request.Url.OriginalString);
				cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    clickId =  reader["clickId"].ToString();
                    landingPage = reader["lp"].ToString();

                    string[] optionalValues = {clickId,newTag,ipaddress,p,campaign,traffic,keyword,affid.ToString(), googleaid, iosifa, deposit_amount.ToString(), fd.ToString()};

                    for(int i=0;i<keys.Length;i++)
                    {
                        landingPage = landingPage.Replace(keys[i], optionalValues[i]);
                    }
                    HttpCookie myCookie = new HttpCookie("aditorcookie");
                    myCookie["clickid"] = clickId;
                    myCookie.Expires = DateTime.Now.AddDays(365d);
                    Response.Cookies.Add(myCookie);

                    Response.Redirect("redirect.aspx?url=" + Server.UrlEncode(landingPage));
                }
                reader = null;
            }
        }



    }
    

}