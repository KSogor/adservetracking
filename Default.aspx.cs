﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void loginAction_Click(object sender, EventArgs e)
    {
        bool loggedIn=false;
        if (usertype.SelectedValue == "0")
            loggedIn = Affiliates.checkLogin(username.Text, password.Text);
        else
            loggedIn = advertisers.checkLogin(username.Text, password.Text);
        
        if (loggedIn)
            Response.Redirect("/partners/");

    }
}