﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class click : System.Web.UI.Page
{
    public string[] keys = {"{aff}","{tag}","{keyword}","{traffic}","{campaign}","{creative}"};
    private string getFromQuery(string key)
    {

        if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string tag = getFromQuery("tag");
	string aff = getFromQuery("aff");
        string offer= getFromQuery("offer");
        string keyword = getFromQuery("keyword");
        string traffic = getFromQuery("traffic");
        string campaign = getFromQuery("campaign");
        string creative = getFromQuery("creative");   

        string[] optionalValues = {aff,tag,keyword,traffic,campaign,creative};
		string landingPage="http://ads2.aditor.com/click.aspx?aff={aff}&tag={tag}&keyword={keyword}&traffic={traffic}&campaign={campaign}&creative={creative}";
        for(int i=0;i<keys.Length;i++)
        {
            landingPage = landingPage.Replace(keys[i], optionalValues[i]);
        }

        int[] offers = {137};

	Random rnd = new Random();
	int offerid = rnd.Next(0, offers.Length);
    Response.Redirect("../../redirect.aspx?url=" + Server.UrlEncode(landingPage + "&offer=" + offers[offerid]));
    }
    

}