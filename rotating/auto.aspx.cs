﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class click : System.Web.UI.Page
{
	static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public string[] keys = {"{aff}","{tag}","{keyword}","{traffic}","{campaign}","{creative}"};
    static string domainAddress = System.Configuration.ConfigurationManager.AppSettings["domainAddress"].ToString();

    private string getFromQuery(string key)
    {

        if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string tag = getFromQuery("tag");
		string aff = getFromQuery("aff");
        string keyword = getFromQuery("keyword");
        string traffic = getFromQuery("traffic");
        string campaignStr = getFromQuery("campaign");
        string creative = getFromQuery("creative");

        string[] optionalValues = { aff, tag, keyword, traffic, campaignStr, creative };
        string landingPage = "http://" + domainAddress + "/click.aspx?aff={aff}&tag={tag}&keyword={keyword}&traffic={traffic}&campaign={campaign}&creative={creative}";
        for(int i=0;i<keys.Length;i++)
        {
            landingPage = landingPage.Replace(keys[i], optionalValues[i]);
        }

        int campaignId = 0;
        Campaign campaign;
        if (!int.TryParse(campaignStr, out campaignId))
        {
            //TODO get campaign and check whether it is default (by using property)
            logwriter.write(string.Format("{0}| Wrong campaign Id | {1}", DateTime.Now, Server.HtmlEncode(Request.RawUrl)), "wrongRequests.txt");
            campaign = Campaign.GetDefault();
        }
        else
        {
            campaign = Campaign.getCampaign(campaignId);
            if (campaign.CampaignId == 0)
            {
                logwriter.write(string.Format("{0}| Campaign with Id {1} not found | {2}", DateTime.Now, campaignId, Server.HtmlEncode(Request.RawUrl)), "wrongRequests.txt");
                campaign = Campaign.GetDefault();
            }
        }

        int offerId = GetOffer(campaign);

        Response.Redirect("../redirect.aspx?url=" + Server.UrlEncode(landingPage + "&offer=" + offerId));
    }

    private int GetOffer(Campaign campaign)
    {
        var rules = CampaignRule.getAll(campaign.CampaignId);
        //detect country
        var country = GetCountry();
        //get rule for country or default rule
        var rule = rules.FirstOrDefault(r => (r.GeoTag == country || r.GeoTag == "ALL") && !r.RuleIsDefault);
        if (rule == null)
        {
            logwriter.write(string.Format("{0}| Rule for country {1} not found | {2}", DateTime.Now, country, Server.HtmlEncode(Request.RawUrl)), "wrongRules.txt");
            rule = rules.FirstOrDefault(r => r.RuleIsDefault);
        }

        //sum all percentage for rule
        int totalPercent = 0;
        foreach (int p in rule.Offers.Values)
        {
            totalPercent += p;
        }
        //get random offer
        Random rnd = new Random();
        int randomOffer = rnd.Next(1, totalPercent + 1);
        var sum = 0;
        foreach (var op in rule.Offers)
        {
            sum += op.Value;
            if (sum >= randomOffer)
            {
                return op.Key;
            }
        }

        logwriter.write(string.Format("{0}| Failed to generate offerId. Rule:{1} | {2}", DateTime.Now, rule.RuleId, Server.HtmlEncode(Request.RawUrl)), "errors.txt");
            
        return -1;
    }
	
	private string GetCountry(){
		string ipaddress = Request.ServerVariables["remote_addr"].ToString();
        int[] parsedIp = ipaddress.Split('.').Select(h=>Int32.Parse(h)).ToArray();
        int ip = parsedIp[0] * 256 * 256 * 256 + parsedIp[1] * 256 * 256 + parsedIp[2] * 256 + parsedIp[3];
		using (SqlConnection con = new SqlConnection(connectionString))
        {
                using (SqlCommand cmd = new SqlCommand("CountryByIPAddress", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@ip", ipaddress);
					
					SqlParameter prmCountryName = new SqlParameter("@prmCountryName", SqlDbType.VarChar);
					prmCountryName.Direction = ParameterDirection.ReturnValue;
					cmd.Parameters.Add(prmCountryName);

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
					return Convert.ToString(prmCountryName.Value);
                }
        }
	}
}