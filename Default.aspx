﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="homepage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="loginForm">

        <div>Username:</div>
        <div><asp:TextBox ID="username" runat="server"></asp:TextBox></div>
    
        <div>Password:</div>
        <div><asp:TextBox ID="password" TextMode="Password" runat="server"></asp:TextBox></div>
    <div>Account Type:</div>
        <div><asp:DropDownList runat="server" ID="usertype">
            <asp:ListItem Value="0">Affiliate</asp:ListItem>
            <asp:ListItem Value="1">Advertiser</asp:ListItem>
            <asp:ListItem Value="0">Admin</asp:ListItem>
        </asp:DropDownList></div>

        <div><asp:Button runat="server" id="loginAction" value="Login" Text="Login" OnClick="loginAction_Click"/></div>

    </div>

</asp:Content>