﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        lut lutdata = new lut();
        Application.Lock();
        Application.Add("deals", lutdata.deals);
        Application.Add("categories", lutdata.categories);
        Dictionary<int, Hashtable> TagsTable = new Dictionary<int,Hashtable>();
        tags.Load(TagsTable);
        Application.Add("Tags", TagsTable);
        
        Application.UnLock();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        Exception exc = Server.GetLastError();
        logwriter.write(exc, "errors.txt");
        Response.Clear();
        Response.Write("<br/><br/><center>An error has occured and logged in our system for further investigation.</center>"+exc.ToString());
        Server.ClearError();
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
