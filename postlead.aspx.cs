﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class postlead : System.Web.UI.Page
{
    static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;
    public string[] keys = { "{clickid}", "{tag}", "{ipaddress}" };
    private string getFromQuery(string key)
    {

        if (Request.Form[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.Form[key].ToString());
        else if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    private int getIntFromQuery(string key)
    {

        if (Request.Form[key] != null)
            return Convert.ToInt32(HttpContext.Current.Server.UrlEncode(Request.Form[key].ToString()));
        else if (Request.QueryString[key] != null)
            return Convert.ToInt32(HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString()));
        else
            return 0;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string ipaddress = Request.ServerVariables["remote_addr"].ToString();
        int[] parsedIp = ipaddress.Split('.').Select(h => Int32.Parse(h)).ToArray();
        int ip = parsedIp[0] * 256 * 256 * 256 + parsedIp[1] * 256 * 256 + parsedIp[2] * 256 + parsedIp[3];

        int affid = 0;
        int.TryParse(getFromQuery("aff"), out affid);

        affid = (affid == null) ? 0 : affid;
        string tagId = getFromQuery("tag");

        int offerId = getIntFromQuery("offer");
        string p = getFromQuery("clickid");
        string keyword = getFromQuery("keyword");
        string traffic = getFromQuery("traffic");
        string campaign = getFromQuery("campaign");
        string creative = getFromQuery("creative");

        string referer = (Request.ServerVariables["HTTP_REFERER"] != null) ? Request.ServerVariables["HTTP_REFERER"].ToString() : "";
        string landingPage = "";
        string clickId = "";
        string newTag = "0";

        string mCountry = getFromQuery("country");
        string mPostalcode = getFromQuery("postalcode");
        string mState = getFromQuery("state");
        string mCity = getFromQuery("city");
        string mStreet = getFromQuery("street");

        string clientipaddress = getFromQuery("ipaddress");
        if (clientipaddress == "") clientipaddress = ipaddress;
        string mBirthday = getFromQuery("birthday");
        string mBirthmonth = getFromQuery("birthmonth");
        string mBirthyear = getFromQuery("birthyear");
        string mPhone = getFromQuery("phone");
        string mGender = getFromQuery("gender");
        string mFullname = getFromQuery("fullname");
        string mEmail = getFromQuery("email");
        Response.Clear();
        if(mFullname=="")
        {
            
            Response.Write("fullname is missing");
            return;
        }
        if (mPhone == "")
        {
            Response.Write("phone is missing");
            return;
        }
        if (mEmail == "")
        {
            Response.Write("email is missing");
            return;
        }

        if (tagId.Length > 50) tagId.Substring(0, 49);
        if (tagId != "")
        {
            lock (Application)
            {
                Dictionary<int, Hashtable> TagsTable = (Dictionary<int, Hashtable>)Application["Tags"];
                newTag = tags.Find(TagsTable, affid, tagId);
                Application["Tags"] = TagsTable;
            }
        }
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            using (SqlCommand cmd = new SqlCommand("ClickSave", con))
            {
                SqlDataReader reader;
                cmd.Parameters.AddWithValue("@ipaddress", ipaddress);
                cmd.Parameters.AddWithValue("@affid", affid);
                cmd.Parameters.AddWithValue("@offerid", offerId);
                cmd.Parameters.AddWithValue("@params", p);
                cmd.Parameters.AddWithValue("@referer", referer);
                cmd.Parameters.AddWithValue("@tagid", Convert.ToInt32(newTag));
                cmd.Parameters.AddWithValue("@creative", creative);
                cmd.Parameters.AddWithValue("@keyword", keyword);
                cmd.Parameters.AddWithValue("@trafficsource", traffic);
                cmd.Parameters.AddWithValue("@campaign", campaign);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    clickId = reader["clickId"].ToString();
                    landingPage = reader["lp"].ToString();
                    string result = savelead.saveLeadToDb(clickId, mFullname, mEmail, mPhone, mGender, mBirthday, mBirthmonth, mBirthyear, mStreet, mCity, mState, mPostalcode, mCountry, clientipaddress);
                    Response.Write(result);
                }
                reader = null;
            }
        }



    }
}