﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{
    public partial class Partners_rules : System.Web.UI.Page
    {
        private int campaignId;
        private bool campaignIsDefault;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!permissions.hasPermission("offers"))
            //{
            //    Response.Clear();
            //    Response.Redirect("/");
            //}

            campaignId = getIntFromQuery("campId");
            campaignIsDefault = getBoolFromQuery("isdefault");

            if (!IsPostBack)
            {
                campaignIsDefaultParam.Value = campaignIsDefault.ToString();
                ShowAllCampaignRules(campaignId);
                InitializeHelper();
            }

            
            //fill form fields

            if (utils.isManager() && !campaignIsDefault)
            {
                managerPanel.Visible = true;
            }

            //if (!IsPostBack)
            //{
            //    //populateFields();
            //}


        }

        private void InitializeHelper()
        {
            List<offers> allOffers = offers.getOffer(0);
            var offerIds = allOffers.Select(o => o.offerId.ToString()).ToList();
            var offerNames  = allOffers.Select(o => o.offername).ToList();
            offerIdsHelper.Value = "{\"offerIds\":[" + string.Join(",", offerIds) + "],\"offerNames\":[\""+string.Join("\",\"",offerNames)+"\"]}";

            List<Country> allCountries = Countries.getCountryCodes();
            List<string> countryCodes = allCountries.Select(c => "\"" + c.countryCode + "\"").ToList();
            List<string> countryNames = allCountries.Select(c => "\"" + c.countryName + "\"").ToList();
            countryCodesHelper.Value = "{\"countryCodes\":[" + string.Join(",", countryCodes) + "],\"countryNames\":[" + string.Join(",", countryNames) + "]}";
        }


        private int getIntFromQuery(string key)
        {
            if (Request.QueryString[key] != null)
            {
                return Convert.ToInt32(HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString()));
            }
            else
            { 
                return 0; 
            }
        }

        private bool getBoolFromQuery(string key)
        {
            if (Request.QueryString[key] != null)
            {
                return Convert.ToBoolean(HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString()));
            }
            else
            {
                return false;
            }
        }

        [WebMethod]
        public static string getRule(int campaignId, int ruleId)
        {
            CampaignRule rule = CampaignRule.getRule(campaignId, ruleId);
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("\"ruleID\":\"" + utils.cleanForJSON(rule.RuleId.ToString()) + "\",");
            sb.Append("\"ruleDescription\":\"" + utils.cleanForJSON(rule.RuleDescription.ToString()) + "\",");
            sb.Append("\"geoTag\":\"" + utils.cleanForJSON(rule.GeoTag.ToString()) + "\",");
            sb.Append("\"offers\":{");

            var offersList = rule.Offers.Select(o => string.Format("\"{0}\": \"{1}\"", utils.cleanForJSON(o.Key.ToString()), utils.cleanForJSON(o.Value.ToString()))).ToList();
            sb.Append(string.Join(",", offersList));

            sb.Append("},");
            sb.Append("\"ruleIsDefault\":\"" + utils.cleanForJSON(rule.RuleIsDefault.ToString().ToLower()) + "\",");
            sb.Append("}");
            return sb.ToString();
        }

        [WebMethod]
        public static void deleteRule(int campaignId, int ruleId)
        {
            CampaignRule.DeleteCampaignRule(campaignId, ruleId);
        }

        public void ShowAllCampaignRules(int campId)
        {
            List<CampaignRule> campaignRules = new List<CampaignRule>();
            campaignRules = CampaignRule.getAll(campId);
            campaignIdParam.Value = campId.ToString();


            rulesRepeater.DataSource = campaignRules;
            rulesRepeater.DataBind();

            rulesPanel.Visible = true;
        }


        protected void addrule_Click(object sender, EventArgs e)
        {
            int campaignId = Convert.ToInt32(campaignIdParam.Value.ToString());
            string campaignIsDefault = campaignIsDefaultParam.Value.ToString();


            if (IsPostBack)
            {
                CampaignRule rule = new CampaignRule();
                rule.RuleId = Convert.ToInt32(ruleID.Value.ToString());
                rule.RuleDescription = ruleDescription.Text.ToString();
                rule.GeoTag = selectedCountryCode.Value.ToString();
                rule.OffersInDbFormat = offersStringToSave.Value.ToString();
                CampaignRule.SaveCampaignRule(campaignId, rule);
                Response.Redirect(string.Format("~/partners/rules.aspx?campId={0}&isdefault={1}", campaignId, campaignIsDefault));
            }

            ShowAllCampaignRules(campaignId);
        }
}
}