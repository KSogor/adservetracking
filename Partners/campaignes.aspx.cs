﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{
    public partial class Partners_campaignes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!permissions.hasPermission("affiliates"))
            {
                Response.Clear();
                Response.Redirect("/");
            }
            if (!IsPostBack)
            { 
                ShowAllCampaignes(); 
            }

            
            //fill form fields

            if (utils.isManager())
            {
                managerPanel.Visible = true;
            }

            //if (!IsPostBack)
            //{
            //    //populateFields();
            //}


        }


        //public void populateFields ()
        //{

        //    offerAdvertiser.DataSource = advertisers.getAdvertiser(0);
        //    offerAdvertiser.DataTextField = "name";
        //    offerAdvertiser.DataValueField = "id";
        //    offerAdvertiser.DataBind();

        //    offerCategory.DataSource = utils.getCategories();
        //    offerCategory.DataValueField = "categoryId";
        //    offerCategory.DataTextField = "categoryName";
        //    offerCategory.DataBind();
        //    offerPayoutType.DataSource = utils.getDeals();
        //    offerPayoutType.DataValueField = "dealId";
        //    offerPayoutType.DataTextField = "dealName";
        //    offerPayoutType.DataBind();
        //    offerRevenueType.DataSource = utils.getDeals();
        //    offerRevenueType.DataValueField = "dealId";
        //    offerRevenueType.DataTextField = "dealName";
        //    offerRevenueType.DataBind();

        //}

        [WebMethod]
        public static string getCampaign(int campaignID)
        {
            Campaign campaign = Campaign.getCampaign(campaignID);
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("\"campaignID\":\"" + utils.cleanForJSON(campaign.CampaignId.ToString()) + "\",");
            sb.Append("\"campaignName\":\"" + utils.cleanForJSON(campaign.CampaignName.ToString()) + "\",");
            sb.Append("\"campaignDescription\":\"" + utils.cleanForJSON(campaign.CampaignDescription.ToString()) + "\",");
            sb.Append("}");
            return sb.ToString();
        }

        public void ShowAllCampaignes()
        {
            List<Campaign> campaignes = new List<Campaign>();
            campaignes = Campaign.getAll();
            campaignesRepeater.DataSource = campaignes;
            campaignesRepeater.DataBind();

            //foreach(RepeaterItem item in offerRepeater.Items)
            //{
            //    Literal category = (Literal)item.FindControl("catName");
            //    category.Text = utils.getCategories()[Offers[item.ItemIndex].category-1].categoryName;

            //    Literal revtype = (Literal)item.FindControl("revType");
            //    revtype.Text = utils.getDeals()[Offers[item.ItemIndex].revenueType-1].dealName;

            //    Literal payouttype = (Literal)item.FindControl("payoutType");
            //    payouttype.Text = utils.getDeals()[Offers[item.ItemIndex].payoutType-1].dealName;


            //}

            campaignesPanel.Visible = true;
        }


        protected void addcampaign_Click(object sender, EventArgs e)
        {
            Campaign campaign = new Campaign();
            campaign.CampaignId = Convert.ToInt32(campaignID.Value.ToString());
            campaign.CampaignName = campaignName.Text.ToString();
            campaign.CampaignDescription = campaignDescription.Text.ToString();
            Campaign.SaveCampaign(campaign);

            ShowAllCampaignes();
            // populateFields();
        }
        
}
}