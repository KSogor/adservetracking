﻿<%@ Page Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" MasterPageFile="~/Partners/MasterPage.master" CodeFile="Advertisers.aspx.cs" Inherits="PageMethods.Partners_Advertisers" %>



<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />

    <div id="screenBlocker"></div>

    <asp:Panel ID="advertiserPanel" Visible="false" runat="server">

        <asp:Repeater runat="server" ID="advertiserRepeater">

            <HeaderTemplate>
                <table class="affiliatesGrid">
                    <tr>
                        <th>adv.ID</th>
                        <th>Advertiser</th>
                        <th>Email</th>

                        <th>Website</th>
                        <th>Api Name(Do Not touch!)</th>
                        <th>PostBack URL</th>
                        <th></th>
                    </tr>
            </HeaderTemplate>


            <ItemTemplate>
                <tr>
                    <td><%# Eval("id") %></td>
                    <td><%# Eval("name") %></td>
                    <td><%# Eval("email") %></td>
                    <td><%# Eval("url") %></td>
                    <td><%# Eval("advertiserPostURL") %></td>
                    <td><%# Eval("advertiserPostBackUrl") %></td>
                    <td><span onclick="editAdvertiser(<%# Eval("id") %>);" style="cursor: pointer;">Edit</span></td>


                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>

    <div class="addEditForm" id="addEditForm">
        <div class="closeWindow" onclick="hidePopup()">X</div>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="advname" class="col-sm-2 control-label">Advertiser</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="advname"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="advurl" class="col-sm-2 control-label">Website</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" TextMode="MultiLine" class="form-control" ID="advurl"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="advemail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="advemail"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="advPassword" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="advPassword"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="advertiserPostURL" class="col-sm-2 control-label">API Name</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="advertiserPostURL"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="advertiserPostBackUrl" class="col-sm-2 control-label">PostBack Url</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" TextMode="MultiLine" class="form-control" ID="advertiserPostBackUrl"></asp:TextBox>
                </div>
            </div>
            <asp:HiddenField ID="advID" runat="server" Value="0" />
            <div>
                <asp:Button runat="server" class="button" ID="addAdvertiser" OnClick="addAdvertiser_Click" Text="Add Account" value="Add Account" />
            </div>
        </div>
    </div>


    <div class="managementPanel">
        <div onclick="clearForm()">Add Advertiser</div>



    </div>

    <script type="text/javascript">
        function editAdvertiser(advID) {
            PageMethods.getAdv(advID, Success, Failure);
            return false;
        }

        function Success(data) {
            var jsonObj = eval("(" + data + ")");
            $("#ContentPlaceHolder1_advname").val(jsonObj.name);
            $("#ContentPlaceHolder1_advurl").val(jsonObj.url);
            $("#ContentPlaceHolder1_advemail").val(jsonObj.email);
            $("#ContentPlaceHolder1_advPassword").val(jsonObj.advpass);
            $("#ContentPlaceHolder1_advID").val(jsonObj.id);
            if (jsonObj.advertiserPostBackUrl)
                $("#ContentPlaceHolder1_advertiserPostBackUrl").val(jsonObj.advertiserPostBackUrl);
            if (jsonObj.advertiserPostURL)
                $("#ContentPlaceHolder1_advertiserPostURL").val(jsonObj.advertiserPostURL);
            $("#ContentPlaceHolder1_addAdvertiser").val("Update Account");
            showForm();
        }

        function Failure(data) {
            alert("Error getting affiliate data");
        }

        function clearForm() {
            $("#ContentPlaceHolder1_advname").val("");
            $("#ContentPlaceHolder1_advurl").val("");
            $("#ContentPlaceHolder1_advemail").val("");
            $("#ContentPlaceHolder1_advPassword").val("");
            $("#ContentPlaceHolder1_advertiserPostBackUrl").val("");
            $("#ContentPlaceHolder1_advertiserPostURL").val("");
            $("#ContentPlaceHolder1_advID").val(0);
            $("#ContentPlaceHolder1_addAdvertiser").val("Add new Account");
            showForm();
        }

        function showForm() {
            document.getElementById("addEditForm").style.display = 'block';
            document.getElementById("screenBlocker").style.display = 'block';
        }
        function hidePopup() {
            document.getElementById("addEditForm").style.display = 'none';
            document.getElementById("screenBlocker").style.display = 'none';
        }
    </script>
</asp:Content>
