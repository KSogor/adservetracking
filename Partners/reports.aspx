﻿<%@ Page Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true"
    MasterPageFile="~/Partners/MasterPage.master" CodeFile="reports.aspx.cs" Inherits="PageMethods.Partners_Reports" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />
    <div id="screenBlocker">
    </div>
    <div style="width: 98%; margin: 0px 10px 10px 10px; border-bottom: 1px solid #e0e0e0;
        padding-top: 10px;" class="reportform">
        <div style="float: left; font-weight: bold; margin-left: 0px; width: 90px;">
            GROUP BY:</div>
        <div style="float: left;">
            <asp:DropDownList ID="groupdate" runat="server">
                <asp:ListItem Value="ALL" Text="No date grouping"></asp:ListItem>
                <asp:ListItem Value="Yearly" Text="Yearly"></asp:ListItem>
                <asp:ListItem Value="monthly" Text="Monthly"></asp:ListItem>
                <asp:ListItem Value="daily" Text="Daily"></asp:ListItem>
                <asp:ListItem Value="dailyhourly" Text="Daily + Hourly"></asp:ListItem>
                <asp:ListItem Value="hourly" Text="Hourly"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:Panel ID="AdminPanel2" runat="server">
            <div style="float: left;">
                <asp:CheckBox Text="Adevrtiser" runat="server" ID="groupadvertiser" /></div>
        </asp:Panel>
        <div style="float: left;">
            <asp:CheckBox Text="Offers" runat="server" ID="groupOffers" /></div>
        <div style="float: left;">
            <asp:CheckBox Text="Affiliate" runat="server" ID="groupaffiliate" /></div>
        <div style="float: left;">
            <asp:CheckBox Text="Site ID(tag)" runat="server" ID="grouptag" /></div>
        <div style="float: left;">
            <asp:CheckBox Text="Geo" runat="server" ID="groupGeo" /></div>
        <div style="float: left; margin-left: 10px;">
            <asp:DropDownList runat="server" ID="dateRange">
            </asp:DropDownList>
        </div>
        <div id="customDates" style="display: none; margin-left: 10px; float: left;">
            <div style="float: left;">
                date from:</div>
            <div style="float: left;">
                <asp:TextBox runat="server" ID="datefrom" CssClass="datepicker"></asp:TextBox></div>
            <div style="float: left; margin-left: 15px;">
                date to:</div>
            <div style="float: left;">
                <asp:TextBox runat="server" ID="dateto" CssClass="datepicker"></asp:TextBox></div>
        </div>
    </div>
    <div style="width: 98%; margin-top: 10px; margin: 10px;" class="reportform">
        <div>
            <div style="float: left; margin-left: 0px; font-weight: bold; width: 90px;">
                Type :
            </div>
            <div>
                <asp:DropDownList runat="server" ID="reportType">
                    <asp:ListItem Value="0" Text="Activity Report"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Report by Clicks"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div style="margin-top:10px;">
        <div style="clear:both; float: left; font-weight: bold; margin-right: 10px;">
            FILTER BY:</div>
        <asp:Panel ID="advertiserPanel" runat="server">
            <div style="float: left; margin-left: 15px;">
                Advertisers :</div>
            <div style="float: left;">
                <asp:ListBox runat="server" SelectionMode="Multiple" OnSelectedIndexChanged="advertisersDropDown_SelectedIndexChanged"
                    ID="advertisersDropDown" AutoPostBack="true"></asp:ListBox>
            </div>
            <div style="float: left; margin-left: 15px;">
                Offer :</div>
            <div style="float: left;">
                <asp:ListBox runat="server" SelectionMode="Multiple" ID="offersDropDown"></asp:ListBox>
            </div>
        </asp:Panel>
        <asp:Panel ID="affiliateDropPanel" runat="server">
            <div style="float: left; margin-left: 15px;">
                Affiliates :</div>
            <div style="float: left;">
                <asp:ListBox runat="server" SelectionMode="Multiple" ID="affiliatesDropDown"></asp:ListBox>
            </div>
        </asp:Panel>
        <asp:Panel ID="geoDropPanel" runat="server">
            <div style="float: left; margin-left: 15px;">
                Geo tag :</div>
            <div style="float: left;">
                <asp:ListBox runat="server" SelectionMode="Multiple" ID="geoDropDown" Style="width: 180px;">
                </asp:ListBox>
            </div>
        </asp:Panel>
        </div>
        <div style="float: right">
            <asp:Button ID="runReport" runat="server" Text="REPORT" OnClick="runReport_Click" /></div>
        <div style="float: right">
            <asp:Button ID="Button1" runat="server" Text="EXCEL" OnClick="runReport_Click" /></div>
    </div>
    <asp:Panel ID="reportPanel" Visible="false" runat="server">
        <div style="width: 100%; clear: both; height: 30px; font-size: 14px; text-align: center;">
            <asp:Literal ID="daterangeTitle" runat="server"></asp:Literal>
        </div>
        <table class="tablesorter" id="myTable" style="width: 100%; clear: both;">
            <asp:Literal runat="server" ID="LiteralReportData"></asp:Literal>
        </table>
        <script type="text/javascript">
            $(document).ready(function () {
                // call the tablesorter plugin 
                $("#myTable").tablesorter({
                    // sort on the first column and third column, order asc 
                    sortList: [[0, 0], [2, 0]]
                });
            });

        </script>
    </asp:Panel>
    <script type="text/javascript">
        var datesSelector = document.getElementById("ContentPlaceHolder1_dateRange");
        datesSelector.onchange = function () {
            if (this.options[this.selectedIndex].value == "0") {
                document.getElementById("customDates").style.display = '';
            }
            else
                document.getElementById("customDates").style.display = 'none';
        }
        datesSelector.onkeyup = datesSelector.onchange;

        datesSelector.onchange();
    </script>
</asp:Content>
