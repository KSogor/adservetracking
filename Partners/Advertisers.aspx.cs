﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PageMethods
{
    public partial class Partners_Advertisers : System.Web.UI.Page
    {
        [WebMethod]
        public static string getAdv(int advertsiderID)
        {
            List<advertisers> adv = advertisers.getAdvertiser(advertsiderID);
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("\"name\":\"" + utils.cleanForJSON(adv[0].name) + "\",");
            sb.Append("\"id\":\"" + utils.cleanForJSON(adv[0].id.ToString()) + "\",");
            sb.Append("\"url\":\"" + utils.cleanForJSON(adv[0].url.ToString()) + "\",");
            sb.Append("\"email\":\"" + utils.cleanForJSON(adv[0].email.ToString()) + "\",");
            sb.Append("\"advertiserPostBackUrl\":\"" + utils.cleanForJSON(adv[0].advertiserPostBackUrl.ToString()) + "\",");
            sb.Append("\"advpass\":\"" + utils.cleanForJSON(adv[0].advertiserpassword.ToString()) + "\",");
            sb.Append("\"advertiserPostURL\":\"" + utils.cleanForJSON(adv[0].advertiserPostURL.ToString()) + "\"");
            sb.Append("}");
            return sb.ToString();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!permissions.hasPermission("advertisers"))
            {
                Response.Clear();
                Response.Redirect("/");
            }
            if (!IsPostBack)
            {
                List<advertisers> Advertisers = new List<advertisers>();
                Advertisers = advertisers.getAdvertiser(0);
                advertiserRepeater.DataSource = Advertisers;
                advertiserRepeater.DataBind();
                advertiserPanel.Visible = true;
            }
        }
        protected void addAdvertiser_Click(object sender, EventArgs e)
        {
            advertisers adv = new advertisers();
            adv.name = advname.Text.ToString();
            adv.email = advemail.Text.ToString();
            adv.url = advurl.Text.ToString();
            adv.advertiserpassword = advPassword.Text.ToString();
            adv.advertiserPostURL = advertiserPostURL.Text.ToString();
            adv.advertiserPostBackUrl = advertiserPostBackUrl.Text.ToString();
            adv.id = Convert.ToInt32(advID.Value.ToString());
            adv.saveAdvertiser(adv);

            List<advertisers> Advertisers = new List<advertisers>();
            Advertisers = advertisers.getAdvertiser(0);
            advertiserRepeater.DataSource = Advertisers;
            advertiserRepeater.DataBind();
            advertiserPanel.Visible = true;
        }
    }
}