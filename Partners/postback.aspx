﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeFile="postback.aspx.cs" Inherits="PageMethods.Partners_PostBack" %>



<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />

    <div style="width: 90%; height: 20px; padding: 20px;">

        <div style="float: left;">
            Affiliates :
            <asp:DropDownList runat="server" ID="affiliatesDropDown" AutoPostBack="true" OnSelectedIndexChanged="affiliatesDropDown_SelectedIndexChanged">
                <asp:ListItem Value="0" Text="Select Affiliate"></asp:ListItem>
            </asp:DropDownList>
        </div>

    </div>

    <div id="screenBlocker"></div>
    <asp:Panel ID="postBackPanel" Visible="false" runat="server">
        <asp:Repeater runat="server" ID="postbackRepeater">

            <HeaderTemplate>
                <table class="affiliatesGrid">
                    <tr>
                        <th>Postback.ID</th>
                        <th>Advertiser</th>
                        <th>Offer Id</th>
                        <th>Category</th>
                        <th>conversionName(no.)</th>
                        <th>PostBack Link</th>
                        <th>Edit</th>
                    </tr>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td><%# Eval("postbackId") %></td>
                    <td><%# Eval("advertiser") %></td>
                    <td><%# Eval("offerId") %></td>
                    <td><%# Eval("category") %></td>
                    <td><%# Eval("conversionName") %></td>
                    <td style="width: 50%"><%# Eval("postbackurl") %></td>
                    <td><span onclick="editPostBack(<%# Eval("postbackId") %>);" style="cursor: pointer;">Edit</span></td>

                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>

            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>

    <div class="addEditForm" id="addEditForm">
        <div class="closeWindow" onclick="hidePopup()">X</div>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="Advertisers" class="col-sm-2 control-label">Advertiser</label>
                <div class="col-sm-10">
                    <asp:DropDownList onchange="updateOffers(this)" runat="server" class="form-control" ID="Advertisers">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="offersDropDown" class="col-sm-2 control-label">Offers</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offersDropDown">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="postbackUrl" class="col-sm-2 control-label">Postback URL</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" TextMode="MultiLine" class="form-control" ID="postbackUrl"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="conversionType" class="col-sm-2 control-label">Conversion Type</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="conversionType">
                    </asp:DropDownList>
                </div>
            </div>
            <asp:HiddenField ID="postbackId" runat="server" Value="0" />
            <div>
                <asp:Button runat="server" ID="addPostback" OnClick="addPostback_Click" Text="Add postback" value="Add Offer" />
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="managerPanel" Visible="false">
        <div class="managementPanel">
            <div onclick="clearForm()">Add PostBack</div>

            <div>Optional Parameters: tag={...} , creative={...} , traffic={...} , campaign={...} , params={...} </div>

        </div>

        <asp:Literal runat="server" ID="offersJS"></asp:Literal>

        <script type="text/javascript">
            function editPostBack(postbackId) {
                PageMethods.getPostBackInfo(postbackId, Success, Failure);
                return false;
            }

            function addOption(dropDown, txt, value) {
                var option = document.createElement("option");
                option.text = txt;
                option.value = value;
                dropDown.add(option);

            }

            function updateOffers(selectElement) {
                if (selectElement.selectedIndex == -1) selectElement.selectedIndex = 0;
                var advId = selectElement.options[selectElement.selectedIndex].value;
                var dropDown = document.getElementById("ContentPlaceHolder1_offersDropDown");
                for (var i = dropDown.options.length; i-- > 0;)
                    dropDown.options[i] = null;
                for (var i = 0; i < offersJS.length; i++) {
                    if (offersJS[i].advertiserID == advId)
                        addOption(dropDown, offersJS[i].offername, offersJS[i].offerID);
                }
            }
            updateOffers(document.getElementById("ContentPlaceHolder1_Advertisers"));

            function updateSelectedValue(dropdown, value) {
                for (var i = 0; i < dropdown.options.length; i++) {
                    if (dropdown.options[i].value == value) {
                        dropdown.selectedIndex = i;
                        break;
                    }
                }
            }


            function Success(data) {
                var jsonObj = eval("(" + data + ")");
                $("#ContentPlaceHolder1_Advertisers").val(jsonObj.advertiserId);
                $("#ContentPlaceHolder1_postbackUrl").val(jsonObj.postbackurl);
                $("#ContentPlaceHolder1_conversionType").val(jsonObj.conversionType);
                $("#ContentPlaceHolder1_postbackId").val(jsonObj.postbackId);

                updateOffers(document.getElementById("ContentPlaceHolder1_Advertisers"));
                var dropDown = document.getElementById("ContentPlaceHolder1_offersDropDown");
                updateSelectedValue(dropDown, jsonObj.offerId);

                $("#ContentPlaceHolder1_addoffer").val("Update PostBack");
                showForm();
            }

            function Failure(data) {
                alert("Error getting affiliate data");
            }

            function clearForm() {
                $("#ContentPlaceHolder1_Advertisers").val(0);
                $("#ContentPlaceHolder1_postbackUrl").val("");
                $("#ContentPlaceHolder1_conversionType").val(0);
                $("#ContentPlaceHolder1_postbackId").val("0");

                var advertisersDropDown = document.getElementById("ContentPlaceHolder1_Advertisers");

                updateOffers(advertisersDropDown);

                $("#ContentPlaceHolder1_addoffer").val("Add new Offer");
                showForm();
            }




            function showForm() {
                document.getElementById("addEditForm").style.display = 'block';
                document.getElementById("screenBlocker").style.display = 'block';
            }
            function hidePopup() {
                document.getElementById("addEditForm").style.display = 'none';
                document.getElementById("screenBlocker").style.display = 'none';
            }
        </script>


    </asp:Panel>


</asp:Content>
