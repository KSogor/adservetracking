﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeFile="linkGenerator.aspx.cs" Inherits="PageMethods.Partners_Tracking" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />
	<div style="margin: 20px 0 20px 0;font-weight:bold;">
		Link generator:
	</div>
    <div class="link-generator-row">
        <div class="link-generator-attr">Affiliate :
        </div>
        <div class="link-generator-value">
            <asp:DropDownList runat="server" ID="affiliatesDropDown" class="value-changed">
                <asp:ListItem Value="0" Text="Select Affiliate"></asp:ListItem>
            </asp:DropDownList>
            </div>
    </div>

    <div class="link-generator-row">
        <div class="link-generator-attr">Campaign :</div>
		<div class="link-generator-value">
            <asp:DropDownList runat="server" ID="campaignsDropDown" class="value-changed">
                <asp:ListItem Value="0" Text="Select Campaign"></asp:ListItem>
            </asp:DropDownList>
			</div>
    </div>
    <div class="link-generator-row">
	<div class="link-generator-attr">
        <asp:Label ID="Label1" runat="server" Text="Tag"></asp:Label>
		</div>
        <div class="link-generator-value"><asp:TextBox ID="tbTag" runat="server" class="value-changed"></asp:TextBox></div>
    </div>
     <div class="link-generator-row">
        <div class="link-generator-attr"><asp:Label ID="Label2" runat="server" Text="Keyword"></asp:Label></div>
        <div class="link-generator-value"><asp:TextBox ID="tbKeyword" runat="server" class="value-changed"></asp:TextBox></div>
    </div>
     <div class="link-generator-row">
        <div class="link-generator-attr"><asp:Label ID="Label3" runat="server" Text="Traffic"></asp:Label></div>
        <div class="link-generator-value"><asp:TextBox ID="tbTraffic" runat="server" class="value-changed"></asp:TextBox></div>
    </div>
     <div class="link-generator-row">
        <div class="link-generator-attr"><asp:Label ID="Label4" runat="server" Text="Creative"></asp:Label></div>
        <div class="link-generator-value"><asp:TextBox ID="tbCreative" runat="server" class="value-changed"></asp:TextBox></div>
    </div>
    <%--<div class="generate-link-btn" onclick="generateLink()">Generate Link </div>--%>
    <div style="padding:15px 0 10px 10px;">
        
        Result Link: <asp:Label ID="lblLinkResult" runat="server" Text="http://"></asp:Label>
    </div>
	<asp:HiddenField ID="domainAddress" runat="server" Value="0" />
    <div id="screenBlocker"></div>
    <script type="text/javascript">
		$(document).on("change", ".value-changed", function(){
			generateLink();
		});
	
        function generateLink() { 
            if($("#ContentPlaceHolder1_affiliatesDropDown").val() == 0)
			{
				$("#ContentPlaceHolder1_lblLinkResult").html("Select affilate from dropdown.");
				$("#ContentPlaceHolder1_lblLinkResult").css("color", "red");
				return false;
			}
			
			if($("#ContentPlaceHolder1_campaignsDropDown").val() == 0)
			{
				$("#ContentPlaceHolder1_lblLinkResult").html("Select campaign from dropdown.");
				$("#ContentPlaceHolder1_lblLinkResult").css("color", "red");
				return false;
			}
			var domainAddressString = $("#ContentPlaceHolder1_domainAddress").val();
			var affilateId = $("#ContentPlaceHolder1_affiliatesDropDown").val();
			var campaignId = $("#ContentPlaceHolder1_campaignsDropDown").val();
			
			var tag = $("#ContentPlaceHolder1_tbTag").val();
			var keyWord = $("#ContentPlaceHolder1_tbKeyword").val();
			var traffic = $("#ContentPlaceHolder1_tbTraffic").val();
			var creative = $("#ContentPlaceHolder1_tbCreative").val();
			
			var resultedLink = "http://" + domainAddressString 
				+ "/rotating/auto.aspx?aff="+affilateId
				+ "&campaign="+campaignId
				+ "&tag="+tag
				+"&keyword="+keyWord
				+"&traffic="+traffic
				+"&creative=" + creative;
			
			$("#ContentPlaceHolder1_lblLinkResult").css("color", "black");
			$("#ContentPlaceHolder1_lblLinkResult").html("<span class='link-generator-result'>"+resultedLink+"</span>");
			
        }

    </script>
</asp:Content>
