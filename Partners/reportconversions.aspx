﻿<%@ Page Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" MasterPageFile="~/Partners/MasterPage.master" CodeFile="reportconversions.aspx.cs" Inherits="PageMethods.reportconversions" %>



<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />
    <div id="screenBlocker"></div>

    <div style="font-weight: bold; margin: 10px 0 10px 10px;">CONVERSIONS REPORT</div>
    <div style="width: 98%; margin-top: 10px; margin: 10px;" class="reportform">
        <div style="float: left; font-weight: bold; margin-right: 10px; width: 85px;">FILTER BY:</div>
        <div style="float: left; ">
            <asp:DropDownList runat="server" ID="dateRange">
            </asp:DropDownList>

            <div id="customDates" style="display: none; margin-top: 15px;">
                <div style="float: left;">date from:</div>
                <div style="float: left;">
                    <asp:TextBox runat="server" ID="datefrom" CssClass="datepicker"></asp:TextBox>
                </div>

                <div style="float: left; margin-left: 15px;">date to:</div>
                <div style="float: left;">
                    <asp:TextBox runat="server" ID="dateto" CssClass="datepicker"></asp:TextBox>
                </div>
            </div>
        </div>

        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Advertisers :</div>
            <asp:DropDownList runat="server" SelectionMode="Single" AutoPostBack="true" ID="advertisersDropDown"></asp:DropDownList>
        </div>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Offer :</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="offersDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Affiliates :</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="affiliatesDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>
        <asp:Panel ID="geoDropPanel" runat="server">
            <div style="float: left; margin-left: 15px;">
                <div style="float: left;">Geo tag :</div>
                <asp:ListBox runat="server" SelectionMode="Multiple" ID="geoDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
            </div>
        </asp:Panel>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Campaign:</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="campaignDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Traffic:</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="traficDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Keyword:</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="keywordDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>
        <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Creative:</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="creativeDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>

    </div>


    <div style="width: 98%; margin: 0px 10px 10px 10px; border-top: 1px solid gray; padding-top: 10px;" class="reportform">
        <div style="float: left; font-weight: bold;">Also show in report the following :</div>
        <div style="float: left;">
            <asp:DropDownList ID="groupdate" runat="server">
                <asp:ListItem Value="monthly" Text="Monthly"></asp:ListItem>
                <asp:ListItem Value="daily" Text="Daily"></asp:ListItem>
                <asp:ListItem Value="dailyhourly" Text="Daily + Hourly"></asp:ListItem>
                <asp:ListItem Value="hourly" Text="Hourly"></asp:ListItem>
            </asp:DropDownList>
        </div>
         <div style="float: left; margin-left: 15px;">
            <div style="float: left;">Show columns:</div>
            <asp:ListBox runat="server" SelectionMode="Multiple" ID="availableColumnsDropDown" CssClass="multiSelectDropDown"></asp:ListBox>
        </div>

<%--        <div style="float: left;">
            <asp:CheckBox Text="Adevrtiser" runat="server" ID="groupadvertiser" />
        </div>
        <div style="float: left;">
            <asp:CheckBox Text="Affiliate" runat="server" ID="groupaffiliate" />
        </div>
        <div style="float: left;">
            <asp:CheckBox Text="Site ID(tag)" runat="server" ID="grouptag" />
        </div>
        <div style="float: left;">
            <asp:CheckBox Text="GEO" runat="server" ID="groupgeo" />
        </div>--%>


        <div style="float: right">
            <asp:Button ID="runReport" runat="server" Text="REPORT" OnClick="runReport_Click" />
        </div>
        <div style="float: right">
            <asp:Button ID="Button1" runat="server" Text="EXCEL" OnClick="runReport_Click" />
        </div>
    </div>


    <asp:Panel ID="reportPanel" Visible="false" runat="server">

        <div style="width: 100%; clear: both; height: 30px; font-size: 14px; text-align: center;">
            <asp:Literal ID="daterangeTitle" runat="server"></asp:Literal>
        </div>

        <table class="tablesorter" id="myTable" style="clear: both;">
            <asp:Literal runat="server" ID="LiteralReportData"></asp:Literal>
        </table>
        <script type="text/javascript">
            $(document).ready(function () {
                // call the tablesorter plugin 
                $("#myTable").tablesorter({
                    // sort on the first column, order asc 
                    sortList: [[0, 0]]
                });
            });
        </script>
    </asp:Panel>
    <script type="text/javascript">
        var datesSelector = document.getElementById("ContentPlaceHolder1_dateRange");
        datesSelector.onchange = function () {
            if (this.options[this.selectedIndex].value == "0") {
                document.getElementById("customDates").style.display = '';
            }
            else
                document.getElementById("customDates").style.display = 'none';
        }
        datesSelector.onkeyup = datesSelector.onchange;

        datesSelector.onchange();

        $(".multiSelectDropDown").dropdownchecklist({ width: 200, maxDropHeight: 250 });
    </script>
</asp:Content>
