﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{
    public partial class Partners_PostBack : System.Web.UI.Page
    {
        public string domainAddress = ConfigurationManager.AppSettings["domainAddress"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!permissions.hasPermission("postback"))
            {
                Response.Clear();
                Response.Redirect("/");
            }
            if (!IsPostBack)
            {
                getAffiliates();
                postBackPanel.Visible = false;
                populateFields();
            }
        }

        public void getAffiliates()
        {
            List<Affiliates> affiliates = Affiliates.getAffiliate(0);
            ListItem selectDefault = new ListItem("Select Affiliate", "0");
            affiliatesDropDown.DataSource = affiliates;
            affiliatesDropDown.DataValueField = "affiliateId";
            affiliatesDropDown.DataTextField = "affiliateName";
            affiliatesDropDown.DataBind();
            affiliatesDropDown.Items.Insert(0, selectDefault);
        }

        public void populateFields()
        {
            Advertisers.DataSource = advertisers.getAdvertiser(0);
            Advertisers.DataTextField = "name";
            Advertisers.DataValueField = "id";
            Advertisers.DataBind();

            for (int i = 0; i < utils.conversionTypes.Length; i++)
            {
                ListItem item = new ListItem(utils.conversionTypes[i], i.ToString());
                conversionType.Items.Add(item);
            }
        }

        [WebMethod]
        public static string getAllOffers()
        {
            List<offers> offer = offers.getOffer(0);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type=\"text/javascript\">");
            sb.Append("var offersJS = [");
            for (int i = 0; i < offer.Count; i++)
            {
                // sb.Append(utils.cleanForJSON(offer[i].offerId.ToString())+ ": {");
                sb.Append("{\"offerID\":\"" + utils.cleanForJSON(offer[i].offerId.ToString()) + "\",");
                sb.Append("\"offername\":\"" + utils.cleanForJSON(offer[i].offername.ToString()) + "\",");
                sb.Append("\"advertiserID\":\"" + utils.cleanForJSON(offer[i].advertiserId.ToString()) + "\",");
                sb.Append("\"lp\":\"" + utils.cleanForJSON(offer[i].landingPage.ToString()) + "\",");
                sb.Append("\"category\":\"" + utils.cleanForJSON(offer[i].category.ToString()) + "\",");
                sb.Append("\"revenuetype\":\"" + utils.cleanForJSON(offer[i].revenueType.ToString()) + "\",");
                sb.Append("\"revenuevalue\":\"" + utils.cleanForJSON(offer[i].revenueValue.ToString()) + "\",");
                sb.Append("\"payouttype\":\"" + utils.cleanForJSON(offer[i].payoutType.ToString()) + "\",");
                sb.Append("\"payoutvalue\":\"" + utils.cleanForJSON(offer[i].payoutValue.ToString()) + "\",");
                sb.Append("\"staticvalues\":\"" + utils.cleanForJSON(offer[i].staticvalues.ToString()) + "\",");
                sb.Append("\"active\":\"" + utils.cleanForJSON(offer[i].active.ToString()) + "\"");
                sb.Append("}");
                if (i < offer.Count - 1)
                    sb.Append(",");
            }
            sb.Append("];</script>");

            return sb.ToString();
        }

        [WebMethod]
        public static string getPostBackInfo(int postBackId)
        {

            postbacksNew pback = postbacks.loadPostBacks(postBackId);
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"offerId\":\"" + utils.cleanForJSON(pback.offerId.ToString()) + "\",");
            sb.Append("\"advertiserId\":\"" + utils.cleanForJSON(pback.advertiserId.ToString()) + "\",");
            sb.Append("\"conversionType\":\"" + utils.cleanForJSON(pback.conversionType.ToString()) + "\",");
            sb.Append("\"postbackId\":\"" + utils.cleanForJSON(pback.postbackId.ToString()) + "\",");
            sb.Append("\"postbackurl\":\"" + utils.cleanForJSON(pback.postbackurl.ToString()) + "\"");
            sb.Append("}");
            return sb.ToString();
        }

        public void drawPostbacks(int affiliateId)
        {
            offersJS.Text = getAllOffers();

            List<postbacksItem> postbacksList = new List<postbacksItem>();
            postbacks.loadPostBacks(postbacksList, 0, affiliateId);

            postbackRepeater.DataSource = postbacksList;
            postbackRepeater.DataBind();

            postBackPanel.Visible = true;
            if (utils.isManager())
                managerPanel.Visible = true;
        }

        public int dropdownvalue = 0;

        protected void affiliatesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList afflist = sender as DropDownList;
            if (afflist.SelectedValue != "0")
                drawPostbacks(Convert.ToInt32(afflist.SelectedValue));
            else
            {
                managerPanel.Visible = false;
                postBackPanel.Visible = false;
            }
            dropdownvalue = Convert.ToInt32(afflist.SelectedValue);
        }
        protected void addPostback_Click(object sender, EventArgs e)
        {
            postbacksNew pback = new postbacksNew();
            pback.affId = Convert.ToInt32(affiliatesDropDown.SelectedValue);
            pback.conversionType = Convert.ToInt32(conversionType.SelectedValue);
            pback.postbackurl = postbackUrl.Text;
            pback.offerId = Convert.ToInt32(Request.Form["ctl00$ContentPlaceHolder1$offersDropDown"]);
            pback.postbackId = Convert.ToInt32(postbackId.Value);
            postbacks.save(pback);

            drawPostbacks(dropdownvalue);
        }
    }
}


//addPostback
//offersDropDown
//Advertisers
//postbackUrl
//postbackId
//conversionType
