﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{
    public partial class Partners_Tracking : System.Web.UI.Page
    {
        private int aff;
        private int campaign;
        private string tag;
        private string keyword;
        private string traffic;
        private string creative;

        protected void Page_Load(object sender, EventArgs e)
        {
            domainAddress.Value = ConfigurationManager.AppSettings["domainAddress"].ToString();
            if (!IsPostBack)
            {
                getAffiliates();
                getCampaigns();
            }
        }

        public void getAffiliates()
        {
            List<Affiliates> affiliates = Affiliates.getAffiliate(0);
            ListItem selectDefault = new ListItem("Select Affiliate", "0");
            affiliatesDropDown.DataSource = affiliates;
            affiliatesDropDown.DataValueField = "affiliateId";
            affiliatesDropDown.DataTextField = "affiliateName";
            affiliatesDropDown.DataBind();
            affiliatesDropDown.Items.Insert(0, selectDefault);
        }
        public void getCampaigns()
        {
            List<Campaign> campaigns = Campaign.getAll();
            ListItem selectDefault = new ListItem("Select Campaign", "0");
            campaignsDropDown.DataSource = campaigns;
            campaignsDropDown.DataValueField = "CampaignId";
            campaignsDropDown.DataTextField = "CampaignName";
            campaignsDropDown.DataBind();
            campaignsDropDown.Items.Insert(0, selectDefault);
        }

        protected void affiliatesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblLinkResult.Text = GenerateLink();
        }

        protected void campaignsDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblLinkResult.Text = GenerateLink();
        }

        private string GenerateLink()
        {
            if (campaignsDropDown.SelectedValue != "0")
            {
                campaign = Convert.ToInt32(campaignsDropDown.SelectedValue);
            }
            else { campaign = -1; }

            if (affiliatesDropDown.SelectedValue != "0")
            {
                aff = Convert.ToInt32(affiliatesDropDown.SelectedValue);
            }
            else { aff = -1; }

            tag = tbTag.Text;
            keyword = tbKeyword.Text;
            traffic = tbTraffic.Text;
            creative = tbCreative.Text;

            string res = string.Empty;

            if (campaign != -1 && aff != -1)
            {
                res = string.Format("http://{0}/rotating/auto.aspx?aff={1}&campaign={2}&tag={3}&keyword={4}&traffic={5}&creative={6}", domainAddress, aff, campaign, tag, keyword, traffic, creative);
            }
            return res;
        }

    }
}

