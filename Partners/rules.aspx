﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="rules.aspx.cs"
    Inherits="PageMethods.Partners_rules" EnableViewState="true" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" EnableViewState="true" />
    <div id="screenBlocker">
    </div>
    <div>
        <asp:HiddenField ID="campaignIdParam" runat="server" Value="0" />
        <asp:HiddenField ID="campaignIsDefaultParam" runat="server" Value="0" />

    </div>
    <asp:Panel ID="rulesPanel" Visible="false" runat="server">
        <asp:Repeater runat="server" ID="rulesRepeater">
            <HeaderTemplate>
                <table class="campaignesGrid">
                    <tr>
                        <th>rule.ID
                        </th>
                        <th>rule description
                        </th>
                        <th>geo tag
                        </th>
                        <th>offers
                        </th>
                        <%if (utils.isManager())
                            { %>
                        <th>Edit
                        </th>
                        <th>Delete
                        </th>
                        <%} %>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("RuleId") %>
                    </td>
                    <td>
                        <%# Eval("RuleDescription") %>
                    </td>
                    <td>
                        <%# Eval("GeoTag") %>
                    </td>
                    <td>
                        <%# Eval("OffersString") %>
                    </td>
                    <%if (utils.isManager())
                        { %>

                    <td>
                        <span onclick="editrule(<%# Eval("RuleId") %>);" style="cursor: pointer;">Edit</span>
                    </td>
                    <td>
                        <span style="cursor: pointer; display: <%# (Eval("RuleIsDefault").ToString() == "False") ? "block":"none" %>" onclick="deleteRule(<%# Eval("RuleId") %>);" style="cursor: pointer;">Delete</span>
                    </td>
                    <%} %>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <div id="error_message"></div>

    <div class="addEditForm" id="addEditRule">
        <div class="closeWindow" onclick="hideRulePopup()">X</div>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="offername" class="col-sm-2 control-label">Rule description</label>
                <div class="col-sm-10">
                    <asp:TextBox class="form-control" runat="server" ID="ruleDescription"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="offername" class="col-sm-2 control-label">GEO location</label>
                <div class="col-sm-10">
                    <select class="form-control" runat="server" id="ruleGeoTag" onchange="setSelectedCountryCode();"></select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    Offers
                </div>
                <div class=" col-sm-10">
                    <div id="offer-percentage-mapping">
                    </div>
                </div>
            </div>
            <input class="btn btn-default" type="button" value="Add another offer" onclick="addPlaceholderForOfferPercentage(0, 0)" />
            <div id="rules-form-validation">
            </div>

            <asp:HiddenField ID="ruleID" runat="server" Value="0" />
            <asp:HiddenField ID="offersStringToSave" runat="server" Value="0" />
            <asp:HiddenField ID="offerIdsHelper" runat="server" Value="0" />
            <asp:HiddenField ID="countryCodesHelper" runat="server" Value="0" />
            <asp:HiddenField ID="selectedCountryCode" runat="server" Value="0" />


            <div style="float: right;">
                <asp:Button Style="width: 150px;" runat="server" ID="addRule" Text="Add Rule" value="Add Rule" OnClick="addrule_Click" OnClientClick="return ValidateRulesForm();" />
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="managerPanel" Visible="false">
        <div class="managementPanel">
            <div onclick="clearRuleForm()">
                Add rule
            </div>
        </div>


    </asp:Panel>

    <script type="text/javascript">
        var makePersentsEquals = false;
        function setSelectedCountryCode() {
            $("#ContentPlaceHolder1_selectedCountryCode").val($("#ContentPlaceHolder1_ruleGeoTag").val());
        }

        function deleteRule(ruleId) {
            if (confirm("Are you sure you want to delete rule with id " + ruleId + "?")) {
                var campId = $("#ContentPlaceHolder1_campaignIdParam").val();
                PageMethods.deleteRule(campId, ruleId, SuccessDelete, Failure);
                return false;
            }

            return false;
        }

        function SuccessDelete(data) {
            window.location.href = "/partners/rules.aspx?campId=" + $("#ContentPlaceHolder1_campaignIdParam").val();
        }

        function editrule(ruleId) {
            makePersentsEquals = false;
            var campId = $("#ContentPlaceHolder1_campaignIdParam").val();
            PageMethods.getRule(campId, ruleId, Success, Failure);
            return false;
        }

        function Success(data) {
            var jsonObj = eval("(" + data + ")");

            initOfferIdsHelper();
            initOCountryCodesHelper();

            $("#rules-form-validation").html("");
            $("#ContentPlaceHolder1_ruleID").val(jsonObj.ruleID);
            $("#ContentPlaceHolder1_ruleDescription").val(jsonObj.ruleDescription);
            $("#ContentPlaceHolder1_ruleGeoTag").html(countryCodesOptions.join(''));
            $("#ContentPlaceHolder1_ruleGeoTag").val(jsonObj.geoTag);

            if (jsonObj.ruleIsDefault == "true") {
                $("#ContentPlaceHolder1_ruleDescription").attr("readonly", true);
                $("#ContentPlaceHolder1_ruleGeoTag").prop("disabled", "disabled");
            }
            else {
                $("#ContentPlaceHolder1_ruleDescription").attr("readonly", false);
                $("#ContentPlaceHolder1_ruleGeoTag").prop("disabled", false);
            }


            setSelectedCountryCode();
            $("#ContentPlaceHolder1_addRule").val("Update Rule");
            $("#ContentPlaceHolder1_offersStringToSave").val("");

            displayOffers(jsonObj.offers);
            showRuleForm();
        }

        function Failure(data) {
            alert("Error handling rule data");
        }

        function displayOffers(offers) {
            $('#offer-percentage-mapping').html("");

            for (var index in offers) {
                addPlaceholderForOfferPercentage(index, offers[index]);
            }
        }

        var mappingCurrentIndex = 0;
        var options = [];
        var countryCodesOptions = [];

        function initOfferIdsHelper() {
            options = [];
            var offerIdsObj = eval("(" + $("#ContentPlaceHolder1_offerIdsHelper").val() + ")");
            for (i = 0; i < offerIdsObj.offerIds.length; i++) {
                options.push('<option value="' + offerIdsObj.offerIds[i] + '">' + offerIdsObj.offerNames[i] + ' (id=' + offerIdsObj.offerIds[i] + ')' + '</option>');
            }
        }

        function initOCountryCodesHelper() {
            countryCodesOptions = [];
            var countryCodesObj = eval("(" + $("#ContentPlaceHolder1_countryCodesHelper").val() + ")");
            for (i = 0; i < countryCodesObj.countryCodes.length; i++) {
                countryCodesOptions.push('<option value="' + countryCodesObj.countryCodes[i] + '">' + countryCodesObj.countryNames[i] + '</option>');
            }
        }

        function addPlaceholderForOfferPercentage(offerId, percent) {
            mappingCurrentIndex++;

            $('#offer-percentage-mapping').append("<div class='offer-perc-row' id='offer-perc-row_" + mappingCurrentIndex + "'><div class='offer-to-map'><select id='offer-id-" + mappingCurrentIndex + "' style='width:400px;'></select></div><div class='percent-to-map'><input id='offer-percent-" + mappingCurrentIndex + "' type='text' style='width:45px;'/> %</div><div style='float:left;cursor:pointer;' onclick='removeOfferFromRule(this)' id='" + mappingCurrentIndex + "'>remove</div></div>");
            $('#offer-id-' + mappingCurrentIndex).append(options.join(''));
            $('#offer-id-' + mappingCurrentIndex).val(offerId);
            if (percent == 0) {
                $('#offer-percent-' + mappingCurrentIndex).val("1");
            } else {
                $('#offer-percent-' + mappingCurrentIndex).val(percent);
            }

            if (offerId == 0) {
                $('#offer-id-' + mappingCurrentIndex).val($("#offer-id-" + mappingCurrentIndex + " option:first").val());
            }

            if (makePersentsEquals) {
                AdjustPercents();
            } else {

                if (percent == 0 && offerId == 0) {
                    // add offer with percent that is left to 100

                    var percentSum = 0;
                    $("[id^=offer-percent-]").each(function () {
                        if (parseInt($(this).val())) {
                            percentSum += parseInt($(this).val());
                        }

                    });

                    var percLeft = 100 - (percentSum - 1);
                    if (percLeft > 0) {
                        $('#offer-percent-' + mappingCurrentIndex).val(percLeft);
                    }
                }
            }
        }

        function AdjustPercents() {
            var percCount = $("[id^=offer-percent-]").length;
            var div = Math.floor(100 / percCount);
            var rem = 100 % percCount;
            $("[id^=offer-percent-]").each(function () {
                var percentValue = div;
                if (rem > 0) {
                    percentValue++;
                    rem--;
                }

                $(this).val(percentValue);
            });
        }

        function clearRuleForm() {

            makePersentsEquals = true;
            initOfferIdsHelper();
            initOCountryCodesHelper();

            $("#rules-form-validation").html("");
            $("#ContentPlaceHolder1_ruleID").val(0);
            $("#ContentPlaceHolder1_ruleDescription").val("");
            $("#ContentPlaceHolder1_ruleDescription").attr("readonly", false);
            $("#ContentPlaceHolder1_ruleGeoTag").prop("disabled", false);
            $("#ContentPlaceHolder1_ruleGeoTag").html(countryCodesOptions.join(''));
            $("#ContentPlaceHolder1_ruleGeoTag").val($("#ContentPlaceHolder1_ruleGeoTag option:first").val());
            setSelectedCountryCode();
            $('#offer-percentage-mapping').html("");
            $("#ContentPlaceHolder1_offersStringToSave").val("");
            $("#ContentPlaceHolder1_addRule").val("Add new Rule");

            showRuleForm();
        }

        function ValidateRulesForm() {
            $("#rules-form-validation").html("");

            if ($("#ContentPlaceHolder1_ruleGeoTag").val() == null) {
                $("#rules-form-validation").html("Selected country code");
                return false;
            }

            var uniqueOfferId = [];
            var uniqueOffers = true;

            $("[id^=offer-id-]").each(function () {
                var offerId = $(this).val();
                if (uniqueOfferId.indexOf(offerId) < 0) {
                    uniqueOfferId.push(offerId);
                } else {
                    uniqueOffers = false;
                    return false;
                }
            })

            if (!uniqueOffers) {
                $("#rules-form-validation").html("Offer Ids for the rule should be unique");
                return false;
            }


            var percentSum = 0;
            var notEmptyPercInputs = true;

            if (!$("[id^=offer-percent-]").length) {
                $("#rules-form-validation").html("Add at least one offer to the rule");
                return false;
            }


            $("[id^=offer-percent-]").each(function () {
                if (!$(this).val()) {
                    $("#rules-form-validation").html("Map percents for the each offer");
                    notEmptyPercInputs = false;
                    return false;
                }

                if (parseInt($(this).val())) {
                    percentSum += parseInt($(this).val());
                }

            })

            if (notEmptyPercInputs) {
                if (percentSum !== 100) {
                    $("#rules-form-validation").html("Make sure the sum of all percents is 100%");
                    return false;
                }
            }
            else {
                return false;
            }

            formOfferIdPercentString();

            return true;
        };

        function formOfferIdPercentString() {
            var pairs = [];

            $("[id^=offer-id-]").each(function () {
                var offerId = $(this).val();
                var percent = $(this).parent().next().find("input").val();
                pairs.push(offerId + "-" + percent);
            })

            $("#ContentPlaceHolder1_offersStringToSave").val(pairs.join(';'));
        }


        $(document).on("keyup", "[id^=offer-percent-]", function () {
            makePersentsEquals = false;
            var newPerc = $(this).val().replace(/[^0-9]/g, '');
            if (newPerc === '0' || newPerc == '') {
                newPerc = "1";
            }

            $(this).val(newPerc);
        });

        function showRuleForm() {
            document.getElementById("addEditRule").style.display = 'block';
            document.getElementById("screenBlocker").style.display = 'block';
        }


        function hideRulePopup() {
            mappingCurrentIndex = 0;
            document.getElementById("addEditRule").style.display = 'none';
            document.getElementById("screenBlocker").style.display = 'none';
        }


        function removeOfferFromRule(elem) {
            $("#offer-perc-row_" + elem.id).remove();
            if (makePersentsEquals) {
                AdjustPercents();
            }
        }

    </script>
</asp:Content>
