﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false"
    EnableEventValidation="false" AutoEventWireup="true" CodeFile="campaignes.aspx.cs"
    Inherits="PageMethods.Partners_campaignes" %>

<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />
    <div id="screenBlocker">
    </div>
    <asp:Panel ID="campaignesPanel" Visible="false" runat="server">
        <asp:Repeater runat="server" ID="campaignesRepeater">
            <HeaderTemplate>
                <table class="campaignesGrid">
                    <tr>
                        <th>campaign.ID
                        </th>
                        <th>campaign name
                        </th>
                        <th>campaign description
                        </th>
                        <th>rules count
                        </th>
                        <th>Rules Details
                        </th>
                        <%if (utils.isManager())
                            { %>
                        <th>Edit
                        </th>
                        <%} %>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("CampaignId") %>
                    </td>
                    <td>
                        <%# Eval("CampaignName") %>
                    </td>
                    <td>
                        <%# Eval("CampaignDescription") %>
                    </td>
                    <td>
                        <%# Eval("RulesCount") %>
                    </td>
                    <td>
                        <span onclick="redirectToRules(<%# Eval("CampaignId") %>, <%# Eval("CampaignIsDefault").ToString().ToLower() %>);" style="cursor: pointer;">Edit Rules</span>
                    </td>
                    <%if (utils.isManager())
                        { %>

                    <td>
                        <span style="display: <%# (Eval("CampaignIsDefault").ToString() == "False") ? "block":"none" %>" onclick="editcampaign(<%# Eval("CampaignId") %>);" style="cursor: pointer;">Edit</span>
                    </td>
                    <%} %>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <div class="addEditForm" id="addEditForm">
        <div class="form-horizontal">
            <div class="closeWindow" onclick="hidePopup()">X</div>
            <div class="form-group">
                <label for="campaignName" class="col-sm-2 control-label">Campaign name</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="campaignName"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="campaignDescription" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" TextMode="MultiLine" class="form-control" ID="campaignDescription"></asp:TextBox>
                </div>
            </div>
            <asp:HiddenField ID="campaignID" runat="server" Value="0" />
            <div style="float: right;">
                <asp:Button runat="server" ID="addcampaign" OnClick="addcampaign_Click" Text="Add Campaign"
                    value="Add Campaign" />
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="managerPanel" Visible="false">
        <div class="managementPanel">
            <div onclick="clearForm()">
                Add campaign
            </div>
        </div>

        <script type="text/javascript">

            function editcampaign(campaignID) {
                PageMethods.getCampaign(campaignID, Success, Failure);
                return false;
            }

            function Success(data) {
                var jsonObj = eval("(" + data + ")");
                $("#ContentPlaceHolder1_campaignID").val(jsonObj.campaignID);
                $("#ContentPlaceHolder1_campaignName").val(jsonObj.campaignName);
                $("#ContentPlaceHolder1_campaignDescription").val(jsonObj.campaignDescription);

                $("#ContentPlaceHolder1_addcampaign").val("Update Campaign");
                showForm();
            }

            function Failure(data) {
                alert("Error getting campaign data");
            }

            function clearForm() {
                $("#ContentPlaceHolder1_campaignID").val(0);
                $("#ContentPlaceHolder1_campaignName").val("");
                $("#ContentPlaceHolder1_campaignDescription").val("");
                $("#ContentPlaceHolder1_addoffer").val("Add new Campaign");
                showForm();
            }

            function showForm() {
                document.getElementById("addEditForm").style.display = 'block';
                document.getElementById("screenBlocker").style.display = 'block';
            }

            function redirectToRules(campaignId, isDefaultCampagn) {
                window.location.href = "/partners/rules.aspx?campId=" + campaignId + "&isdefault=" + isDefaultCampagn;
            }

            function hidePopup() {
                document.getElementById("addEditForm").style.display = 'none';
                document.getElementById("screenBlocker").style.display = 'none';
            }

            function removeOfferFromRule() {
                $(this).parent('.offer-perc-row').hide();
            }

        </script>
    </asp:Panel>
</asp:Content>
