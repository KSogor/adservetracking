﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PageMethods
{
    public partial class reportleads : System.Web.UI.Page
    {

        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

        public DateTime fromdate = new DateTime();

        [WebMethod]
        public static string getAdv(int advertsiderID)
        {
            
            return "";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!permissions.hasPermission("reportleads"))
            {
                Response.Clear();
                Response.Redirect("/");
            }

            ListItem plzSelect = new ListItem("Please select", "0");

            if (!IsPostBack)
            {
                datefrom.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                dateto.Text = DateTime.Now.ToString("MM/dd/yyyy");

                List<advertisers> adv = new List<advertisers>();
                adv = advertisers.getAdvertiser(0);
                advertisersDropDown.DataSource = adv;
                advertisersDropDown.DataTextField = "name";
                advertisersDropDown.DataValueField = "id";
                advertisersDropDown.DataBind();

                List<Country> codes = new List<Country>();
                codes = Countries.getCountryCodes();
                geoDropDown.DataSource = codes;
                geoDropDown.DataTextField = "countryName";
                geoDropDown.DataValueField = "countryCode";
                geoDropDown.DataBind();

                List<Affiliates> aff = new List<Affiliates>();
                aff = Affiliates.getAffiliate(0);
                affiliatesDropDown.DataSource = aff;
                affiliatesDropDown.DataTextField = "affiliateName";
                affiliatesDropDown.DataValueField = "affiliateId";
                affiliatesDropDown.DataBind();
                affiliatesDropDown.Items.Insert(0, plzSelect);

                DateTime now = DateTime.Now;

                ListItem dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Today";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.AddDays(-1).ToShortDateString() + "~" + DateTime.Now.AddDays(-1).ToShortDateString();
                dateSelection.Text = "Yesterday";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.AddDays(-7).ToShortDateString() + "~" + DateTime.Now.AddDays(-1).ToShortDateString();
                dateSelection.Text = "Last 7 Days";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = new DateTime(now.Year, now.Month, 1).ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "This Month (" + DateTime.Now.ToString("MMMM") + ")";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                DateTime origDT = DateTime.Now.AddMonths(-1);
                DateTime lastDate = new DateTime(origDT.Year, origDT.Month, 1).AddMonths(1).AddDays(-1);
                dateSelection.Value = new DateTime(now.AddMonths(-1).Year, now.AddMonths(-1).Month, 1).ToShortDateString() + "~" + lastDate.ToShortDateString();
                dateSelection.Text = "Last Month (" + DateTime.Now.AddMonths(-1).ToString("MMMM") + ")";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = new DateTime(now.AddMonths(-6).Year, now.AddMonths(-6).Month, 1).ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Last Six Months";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value ="01/01/2014~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Since Ever";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = "0";
                dateSelection.Text = "Custom Date";
                dateRange.Items.Add(dateSelection);
            }
            else if (!string.IsNullOrWhiteSpace(advertisersDropDown.SelectedValue))
            {
                if (ViewState["currentAdvertiser"] == null || (ViewState["currentAdvertiser"] != null && ViewState["currentAdvertiser"].ToString() != advertisersDropDown.SelectedValue))
                {
                    List<offers> myoffers = new List<offers>();
                    myoffers = offers.getOfferByAdvertiser(Convert.ToInt32(advertisersDropDown.SelectedValue));
                    offersDropDown.DataSource = myoffers;
                    offersDropDown.DataTextField = "offername";
                    offersDropDown.DataValueField = "offerId";
                    offersDropDown.DataBind();
                    offersDropDown.Items.Insert(0, plzSelect);
                    ViewState["currentAdvertiser"] = advertisersDropDown.SelectedValue;
                }
            }
           // ViewState["currentAdvertiser"] = null;

        }
        protected void addAdvertiser_Click(object sender, EventArgs e)
        {
   
        }
        protected void runReport_Click(object sender, EventArgs e)
        {
            string selectedOffers = null;
            string selectedAffiliates = null;
            string selectedGeo = null;

            for (int i = 0; i < geoDropDown.Items.Count; i++)
            {
                if (geoDropDown.Items[i].Selected)
                {
                    string selectedGeoString = geoDropDown.Items[i].Value;
                    if (selectedGeoString == "ALL")
                    {
                        selectedGeo = "0";
                        break;
                    }

                    if (selectedGeo == null)
                    {
                        selectedGeo = selectedGeoString;
                    }
                    else
                    {
                        selectedGeo += "," + selectedGeoString;
                    }
                }
            }
            
            
            
            
            for (int i = 0; i < offersDropDown.Items.Count;i++ )
            {
                if (offersDropDown.Items[i].Selected)
                {
                    if (selectedOffers == null)
                        selectedOffers = offersDropDown.Items[i].Value;
                    else
                        selectedOffers += "," + offersDropDown.Items[i].Value;
                }
            }
            for (int i = 0; i < affiliatesDropDown.Items.Count; i++)
            {
                if (affiliatesDropDown.Items[i].Selected)
                {
                    if (selectedAffiliates == null)
                        selectedAffiliates = affiliatesDropDown.Items[i].Value;
                    else
                        selectedAffiliates += "," + affiliatesDropDown.Items[i].Value;
                }
            }
            if (selectedOffers == null) selectedOffers = "0";
            if (selectedAffiliates == null) selectedAffiliates = "0";

            int maxrows = 1000;
            bool excel = false;
            if(((Button)sender).Text=="EXCEL")
            {
                excel = true;
                maxrows = 0;
            }
            string sDateFrom = null;
            string sDateTo = null;

            if(dateRange.SelectedValue!="0")
            {
                string[] sDates = dateRange.SelectedValue.Split('~');
                sDateFrom = sDates[0];
                sDateTo = sDates[1];
            }
            else
            {
                sDateFrom = datefrom.Text;
                sDateTo = dateto.Text;            
            }

            List<string> fieldNames = new List<string>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Report_leads", con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@datefrom", sDateFrom + " 00:00:00");
                    cmd.Parameters.AddWithValue("@dateto", sDateTo + " 23:59:59");
                    cmd.Parameters.AddWithValue("@advertiserid", (string.IsNullOrWhiteSpace(advertisersDropDown.SelectedValue)) ? "0" : advertisersDropDown.SelectedValue);
                    cmd.Parameters.AddWithValue("@offerId", selectedOffers);
                    cmd.Parameters.AddWithValue("@affiliate", selectedAffiliates);
                    cmd.Parameters.AddWithValue("@gadvertiser", groupadvertiser.Checked);
                    cmd.Parameters.AddWithValue("@gaffiliate", groupaffiliate.Checked);
                    cmd.Parameters.AddWithValue("@geoTag", selectedGeo);
                    cmd.Parameters.AddWithValue("@ggeo", groupgeo.Checked);
                    cmd.Parameters.AddWithValue("@gtag", grouptag.Checked);
                    cmd.Parameters.AddWithValue("@maxrows", maxrows);
                    cmd.Parameters.AddWithValue("@format", groupdate.SelectedValue);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    reader =cmd.ExecuteReader();
                    for(int i=0;i<reader.FieldCount;i++)
                    {
                        fieldNames.Add(reader.GetName(i));
                    }

                    daterangeTitle.Text = "Report for : " + sDateFrom + " - " + sDateTo;

                    Int64 clicks = 0;
                    Int64 signups = 0;
                    Int64 conversions = 0;
                    Int64 deposits = 0;

                    StringBuilder sb = new StringBuilder();
                    if (!excel)
                    {
                        sb.Append("<thead><tr>");
                        for (int i = 0; i < fieldNames.Count; i++)
                        {
                            sb.Append("<th style=\"width:150px;\">");
                            sb.Append(fieldNames[i]);
                            sb.Append("</th>");
                        }
                        sb.Append("</tr></thead>");
                        sb.Append("<tbody>");
                        while (reader.Read())
                        {
                            sb.Append("<tr>");
                            for (int i = 0; i < fieldNames.Count; i++)
                            {
                                sb.Append("<td>");
                                switch (fieldNames[i])
                                {
                                    case "date":
                                        sb.Append(reader[fieldNames[i]].ToString().Split(' ')[0]);
                                        break;
                                    case "Hour":
                                        sb.Append(reader[fieldNames[i]].ToString() + ":00");
                                        break;
                                    case "month":
                                        string[] date = reader[fieldNames[i]].ToString().Split('/');
                                        sb.Append(date[0] + "/" + date[2].Split(' ')[0]);
                                       break;
                                    default:
                                        sb.Append(reader[fieldNames[i]].ToString());

                                        break;

                                }                                    
                                sb.Append("</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</tbody>");
 
                        LiteralReportData.Text = sb.ToString();
                    }
                    else
                    {
                        for (int i = 0; i < fieldNames.Count; i++)
                        {
                            sb.Append(fieldNames[i]);
                            sb.Append(",");
                        }
                        sb.Append("\n");

                        

                        while (reader.Read())
                        {
                            for (int i = 0; i < fieldNames.Count; i++)
                            {
                                if (fieldNames[i] == "date")
                                    sb.Append(reader[fieldNames[i]].ToString().Split(' ')[0]);
                                else
                                    sb.Append(reader[fieldNames[i]].ToString());
                                sb.Append(",");
                            }
                            sb.Append("\n");

                        }
                        ExportToExcel(sb.ToString());
                    }

                   

                   
                    sb.Clear();
                    sb = null;

                    reader.Close();
                    reader = null;
                }
            }
            
            reportPanel.Visible = true;

        }

        public void ExportToExcel(string excel)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment;filename=report.csv");
            Response.Charset = "";
            this.EnableViewState = false;

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

            Response.Write(excel);
            Response.End();
        }

    }

}