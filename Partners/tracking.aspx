﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeFile="tracking.aspx.cs" Inherits="PageMethods.Partners_Tracking" %>



<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true" 
         EnablePageMethods="true" />
    
     <div style="width:90%;height:20px;padding:20px;">

        <div style="float:left;">Affiliates : <asp:DropDownList runat="server" ID="affiliatesDropDown" AutoPostBack="true" OnSelectedIndexChanged="affiliatesDropDown_SelectedIndexChanged"><asp:ListItem Value="0" Text="Select Affiliate"></asp:ListItem></asp:DropDownList></div>

    </div>

     <div id="screenBlocker"></div>
    <asp:Panel ID="postBackPanel" visible="false" runat="server">
        <asp:Repeater runat="server" ID="offerRepeater">

            <HeaderTemplate>
                <table class="affiliatesGrid">
                    <tr>
                       <th>offer.ID</th>
                        <th>advertiser</th>
                        <th>Offer name</th>
                        <th>Category</th>
                         <th>Offer Affiliate Link</th>
                      
                         </tr>
            </HeaderTemplate>
            
            <ItemTemplate>
                    <tr>
                         <td><%# Eval("offerID") %></td>
                        <td><asp:Literal runat="server" ID="Literal3"></asp:Literal><%# Eval("advertisername") %></td>
                        <td><%# Eval("offername") %></td>
                        <td><asp:Literal runat="server" ID="catName"></asp:Literal></td>
                        <td style="line-height:25px;">
                        http://<%=domainAddress%>/click.aspx?offer=<%# Eval("offerId") %>&aff=<%=dropdownvalue%>

                        </td>


                          </tr>
            </ItemTemplate>

            <FooterTemplate>

                </table>

            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>


</asp:Content>
