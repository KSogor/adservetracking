﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{ 
public partial class Partners_Affiliates : System.Web.UI.Page
{

    [WebMethod]
    public static string getAff(int affiliateID)
    {
        
        List<Affiliates> aff = Affiliates.getAffiliate(affiliateID);
        StringBuilder sb = new StringBuilder();

        sb.Append("{");
        sb.Append("\"name\":\"" + utils.cleanForJSON(aff[0].affiliateName) + "\",");
        sb.Append("\"affiliateCountry\":\"" + utils.cleanForJSON(aff[0].affiliateCountry) + "\",");
        sb.Append("\"affiliateId\":\"" + utils.cleanForJSON(aff[0].affiliateId.ToString()) + "\",");
        sb.Append("\"affiliatePassword\":\"" + utils.cleanForJSON(aff[0].affiliatePassword) + "\",");
        sb.Append("\"affiliateType\":\"" + utils.cleanForJSON(aff[0].affiliateType.ToString()) + "\",");
        sb.Append("\"affiliateEmail\":\"" + utils.cleanForJSON(aff[0].affiliateEmail) + "\"");

        sb.Append("}");


        return sb.ToString();

        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!permissions.hasPermission("affiliates"))
        {
            Response.Clear();
            Response.Redirect("/");
        }
        if(!IsPostBack)
        { 
        List<Affiliates> affiliates = new List<Affiliates>();
        affiliates = Affiliates.getAffiliate(0);

        affiliatesRepeater.DataSource = affiliates;
        affiliatesRepeater.DataBind();
        affiliatesPanel.Visible = true;
        }
        
    }
    protected void addAffiliate_Click(object sender, EventArgs e)
    {
        Affiliates aff = new Affiliates();
        aff.affiliateCountry = Country.Text.ToString();
        aff.affiliateEmail = Email.Text.ToString();
        aff.affiliatePassword = Password.Text.ToString();
        aff.affiliateType = Convert.ToInt32(affType.Text.ToString());

        aff.affiliateId = Convert.ToInt32(affiliateID.Value);
        aff.affiliateName = affname.Text.ToString();
        Affiliates.SaveAffiliate(aff);


        List<Affiliates> affiliates = new List<Affiliates>();
        affiliates = Affiliates.getAffiliate(0);

        affiliatesRepeater.DataSource = affiliates;
        affiliatesRepeater.DataBind();
        affiliatesPanel.Visible = true;
    }



}

}