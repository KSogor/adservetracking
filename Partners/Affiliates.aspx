﻿<%@ Page Language="C#" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/Partners/MasterPage.master" AutoEventWireup="true" CodeFile="Affiliates.aspx.cs" Inherits="PageMethods.Partners_Affiliates" %>

<asp:Content runat="server" ID="affiliatePage" ContentPlaceHolderID="ContentPlaceHolder1">

    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />

    <div id="screenBlocker"></div>

    <asp:Panel ID="affiliatesPanel" Visible="false" runat="server">

        <asp:Repeater runat="server" ID="affiliatesRepeater">

            <HeaderTemplate>
                <table class="affiliatesGrid">
                    <tr>
                        <th>Aff.ID</th>
                        <th>Affiliate</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Country</th>
                        <th>Account Type</th>
                    </tr>
            </HeaderTemplate>


            <ItemTemplate>
                <tr>
                    <td><%# Eval("affiliateID") %></td>
                    <td><%# Eval("affiliateName") %></td>
                    <td><%# Eval("affiliateEmail") %></td>
                    <td><span onclick="editAffiliate(<%# Eval("affiliateID") %>);" style="cursor: pointer;">Edit</span></td>
                    <td><%# Eval("affiliateCountry") %></td>
                    <td><%# Eval("affiliateType").ToString().Replace("0", "Affiliate").Replace("1", "Master") %></td>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>

    <div class="addEditForm" id="addEditForm">
        <div class="closeWindow" onclick="hidePopup()">X</div>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="affname" class="col-sm-2 control-label">Affiliate</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="affname"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="Country" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="Country"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="Email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="Email"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="Password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="Password"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="affType" class="col-sm-2 control-label">Permissions</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="affType">
                        <asp:ListItem Value="0" Text="Affiliate"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Manager"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <asp:HiddenField ID="affiliateID" runat="server" Value="0" />
            <div>
                <asp:Button runat="server" ID="addAffiliate" OnClick="addAffiliate_Click" Text="Add Account" value="Add Account" />
            </div>
        </div>
    </div>


    <div class="managementPanel">
        <div onclick="clearForm()">Add Affiliate</div>



    </div>

    <script type="text/javascript">
        function editAffiliate(affID) {
            PageMethods.getAff(affID, Success, Failure);
            return false;
        }

        function Success(data) {
            var jsonObj = eval("(" + data + ")");
            $("#ContentPlaceHolder1_affname").val(jsonObj.name);
            $("#ContentPlaceHolder1_Country").val(jsonObj.affiliateCountry);
            $("#ContentPlaceHolder1_Email").val(jsonObj.affiliateEmail);
            $("#ContentPlaceHolder1_Password").val(jsonObj.affiliatePassword);
            $("#ContentPlaceHolder1_affType").val(jsonObj.affiliateType);
            $("#ContentPlaceHolder1_affiliateID").val(jsonObj.affiliateId);
            showForm();
        }

        function Failure(data) {
            alert("Error getting affiliate data");
        }

        function clearForm() {
            $("#ContentPlaceHolder1_affname").val("");
            $("#ContentPlaceHolder1_Country").val("");
            $("#ContentPlaceHolder1_Email").val("");
            $("#ContentPlaceHolder1_Password").val("");
            $("#ContentPlaceHolder1_affType").val(0);
            $("#ContentPlaceHolder1_affiliateID").val(0);
            showForm();
        }

        function showForm() {
            document.getElementById("addEditForm").style.display = 'block';
            document.getElementById("screenBlocker").style.display = 'block';
        }
        function hidePopup() {
            document.getElementById("addEditForm").style.display = 'none';
            document.getElementById("screenBlocker").style.display = 'none';
        }
    </script>


</asp:Content>
