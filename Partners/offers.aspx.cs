﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PageMethods
{
    public partial class Partners_offers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!permissions.hasPermission("offers"))
            {
                Response.Clear();
                Response.Redirect("/");
            }
            if (!IsPostBack)
                drawOffers();

            
            //fill form fields

            if (utils.isManager())
                managerPanel.Visible = true;

            if (!IsPostBack)
            {
                populateFields();
            }


        }


        public void populateFields ()
        {

            offerAdvertiser.DataSource = advertisers.getAdvertiser(0);
            offerAdvertiser.DataTextField = "name";
            offerAdvertiser.DataValueField = "id";
            offerAdvertiser.DataBind();

            offerCategory.DataSource = utils.getCategories();
            offerCategory.DataValueField = "categoryId";
            offerCategory.DataTextField = "categoryName";
            offerCategory.DataBind();
            offerPayoutType.DataSource = utils.getDeals();
            offerPayoutType.DataValueField = "dealId";
            offerPayoutType.DataTextField = "dealName";
            offerPayoutType.DataBind();
            offerRevenueType.DataSource = utils.getDeals();
            offerRevenueType.DataValueField = "dealId";
            offerRevenueType.DataTextField = "dealName";
            offerRevenueType.DataBind();

        }

        [WebMethod]
        public static string getAdv(int offerID)
        {
            List<offers> offer = offers.getOffer(offerID);
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("\"offerID\":\"" + utils.cleanForJSON(offer[0].offerId.ToString()) + "\",");
            sb.Append("\"advertiserID\":\"" + utils.cleanForJSON(offer[0].advertiserId.ToString()) + "\",");
            sb.Append("\"lp\":\"" + utils.cleanForJSON(offer[0].landingPage.ToString()) + "\",");
            sb.Append("\"category\":\"" + utils.cleanForJSON(offer[0].category.ToString()) + "\",");
            sb.Append("\"revenuetype\":\"" + utils.cleanForJSON(offer[0].revenueType.ToString()) + "\",");
            sb.Append("\"revenuevalue\":\"" + utils.cleanForJSON(offer[0].revenueValue.ToString()) + "\",");
            sb.Append("\"payouttype\":\"" + utils.cleanForJSON(offer[0].payoutType.ToString()) + "\",");
            sb.Append("\"payoutvalue\":\"" + utils.cleanForJSON(offer[0].payoutValue.ToString()) + "\",");
            sb.Append("\"staticvalues\":\"" + utils.cleanForJSON(offer[0].staticvalues.ToString()) + "\",");
            sb.Append("\"active\":\"" + utils.cleanForJSON(offer[0].active.ToString()) + "\",");
            sb.Append("\"offername\":\"" + utils.cleanForJSON(offer[0].offername.ToString()) + "\"");
            sb.Append("}");
            return sb.ToString();
        }

        public void drawOffers()
        {
            List<offers> Offers = new List<offers>();
            Offers = offers.getOffer(0);
            foreach (var offer in Offers) {
                offer.landingPage = offer.landingPage.Replace("&", "&amp;");
            }

            offerRepeater.DataSource = Offers;
            offerRepeater.DataBind();

            foreach(RepeaterItem item in offerRepeater.Items)
            {
                Literal category = (Literal)item.FindControl("catName");
                category.Text = utils.getCategories()[Offers[item.ItemIndex].category-1].categoryName;

                Literal revtype = (Literal)item.FindControl("revType");
                revtype.Text = utils.getDeals()[Offers[item.ItemIndex].revenueType-1].dealName;

                Literal payouttype = (Literal)item.FindControl("payoutType");
                payouttype.Text = utils.getDeals()[Offers[item.ItemIndex].payoutType-1].dealName;


            }

            offersPanel.Visible = true;
        }


        protected void addoffer_Click(object sender, EventArgs e)
        {
            offers off = new offers();
            off.landingPage = landingPage.Text.ToString();
            off.advertiserId = Convert.ToInt32(offerAdvertiser.SelectedValue.ToString());
            off.active = Convert.ToBoolean(offerActive.SelectedValue.ToString());
            off.category = Convert.ToInt32(offerCategory.SelectedValue.ToString());
            off.payoutType = Convert.ToInt32(offerPayoutType.SelectedValue.ToString());
            off.revenueType = Convert.ToInt32(offerRevenueType.SelectedValue.ToString());
            off.revenueValue = Convert.ToInt32(offerRevenueValue.Text.ToString());
            off.payoutValue = Convert.ToInt32(offerPayoutValue.Text.ToString());
            off.offerId = Convert.ToInt32(offerID.Value.ToString());
            off.staticvalues = staticvalues.Text.ToString();
            off.offername = offername.Text.ToString();
            offers.SaveOffer(off);

            drawOffers();
           // populateFields();
        }
        
}
}