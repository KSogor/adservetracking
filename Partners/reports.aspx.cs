﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PageMethods
{
    public partial class Partners_Reports : System.Web.UI.Page
    {

        static string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString;

        public DateTime fromdate = new DateTime();

        [WebMethod]
        public static string getAdv(int advertsiderID)
        {
            
            return "";
        }
        public advertisers advertiser = null;
        public Affiliates affiliate = null;

        public void adminPermissions(bool admin)
        {

            advertiserPanel.Visible = admin;
            AdminPanel2.Visible = admin;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            advertiserPanel.Visible = false;
            int advertiserId = 0;
            

            if (Session["advertiser"] != null)
            {
                advertiser = (advertisers)Session["advertiser"];
                advertiserId = advertiser.id;
                adminPermissions(false);
            }
            else if(permissions.isAffiliate())
            {
                adminPermissions(true);
                affiliateDropPanel.Visible = false;
                affiliate = (Affiliates)Session["affiliate"];
            }
            else 
                adminPermissions(true);

            if (!IsPostBack)
            {
                datefrom.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                dateto.Text = DateTime.Now.ToString("MM/dd/yyyy");

                ListItem plzSelect = new ListItem("Please select", "0");

                List<Country> codes = new List<Country>();
                codes = Countries.getCountryCodes();
                geoDropDown.DataSource = codes;
                geoDropDown.DataTextField = "countryName";
                geoDropDown.DataValueField = "countryCode";
                geoDropDown.DataBind();

                List<advertisers> adv = new List<advertisers>();
                adv = advertisers.getAdvertiser(advertiserId);
                advertisersDropDown.DataSource = adv;
                advertisersDropDown.DataTextField = "name";
                advertisersDropDown.DataValueField = "id";
                advertisersDropDown.DataBind();
                
                if (advertiserId == 0)
                    advertisersDropDown.Items.Insert(0, plzSelect);
                else
                    advertisersDropDown.SelectedIndex = 0;

                List<Affiliates> aff = new List<Affiliates>();
                aff = Affiliates.getAffiliate(0);
                affiliatesDropDown.DataSource = aff;
                if (advertiserId==0)
                    affiliatesDropDown.DataTextField = "affiliateName";
                else
                    affiliatesDropDown.DataTextField = "affiliateId";
                affiliatesDropDown.DataValueField = "affiliateId";
                affiliatesDropDown.DataBind();

                DateTime now = DateTime.Now;

                ListItem dateSelection = new ListItem();
                dateSelection.Value = now.AddHours(-1).ToString("MM/dd/yyyy HH:mm:ss") + "~" + now.ToString("MM/dd/yyyy HH:mm:ss");
                dateSelection.Text = "Real Time (Last 60 Mins)";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Today";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.AddDays(-1).ToShortDateString() + "~" + DateTime.Now.AddDays(-1).ToShortDateString();
                dateSelection.Text = "Yesterday";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = DateTime.Now.AddDays(-7).ToShortDateString() + "~" + DateTime.Now.AddDays(-1).ToShortDateString();
                dateSelection.Text = "Last 7 Days";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = new DateTime(now.Year, now.Month, 1).ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "This Month (" + DateTime.Now.ToString("MMMM") + ")";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                DateTime origDT = DateTime.Now.AddMonths(-1);
                DateTime lastDate = new DateTime(origDT.Year, origDT.Month, 1).AddMonths(1).AddDays(-1);
                dateSelection.Value = new DateTime(now.AddMonths(-1).Year, now.AddMonths(-1).Month, 1).ToShortDateString() + "~" + lastDate.ToShortDateString();
                dateSelection.Text = "Last Month (" + DateTime.Now.AddMonths(-1).ToString("MMMM") + ")";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = new DateTime(now.AddMonths(-6).Year, now.AddMonths(-6).Month, 1).ToShortDateString() + "~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Last Six Months";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value ="01/01/2014~" + DateTime.Now.ToShortDateString();
                dateSelection.Text = "Since Ever";
                dateRange.Items.Add(dateSelection);

                dateSelection = new ListItem();
                dateSelection.Value = "0";
                dateSelection.Text = "Custom Date";
                dateRange.Items.Add(dateSelection);
            }
           
        }
        protected void addAdvertiser_Click(object sender, EventArgs e)
        {
   
        }
        protected void runReport_Click(object sender, EventArgs e)
        {
            string selectedAdvertisers=null;
            string selectedAffiliates = null;
            string selectedCountries = null;

            for (int i = 0; i < advertisersDropDown.Items.Count;i++ )
            {
                if(advertisersDropDown.Items[i].Selected)
                {
                    if (selectedAdvertisers == null)
                        selectedAdvertisers = advertisersDropDown.Items[i].Value;
                    else
                        selectedAdvertisers+= "," + advertisersDropDown.Items[i].Value;
                }
            }
            for (int i = 0; i < affiliatesDropDown.Items.Count; i++)
            {
                if (affiliatesDropDown.Items[i].Selected)
                {
                    if (selectedAffiliates == null)
                        selectedAffiliates = affiliatesDropDown.Items[i].Value;
                    else
                        selectedAffiliates += "," + affiliatesDropDown.Items[i].Value;
                }
            }

            for (int i = 0; i < geoDropDown.Items.Count; i++)
            {
                if (geoDropDown.Items[i].Selected)
                {
                    if (selectedCountries == null)
                        selectedCountries = "'"+geoDropDown.Items[i].Value+"'";
                    else
                        selectedCountries += "," + "'" + geoDropDown.Items[i].Value + "'";
                }
            }

            string spName = "Report_Basic_Optimized";
            bool includeCountry = false;

            if (selectedAdvertisers == null) selectedAdvertisers = "0";
            if (selectedAffiliates == null) selectedAffiliates = "0";
            if ((selectedCountries == null || selectedCountries.StartsWith("'ALL'"))) 
            {
                selectedCountries = "";
            }
            else
            {
                spName = "Report_Basic_Optimized";
                includeCountry = true;
            }

            if (groupGeo.Checked)
            {
                spName = "Report_Basic_Optimized";
                includeCountry = true;
            }

            if(affiliate!=null)
            {
                selectedAffiliates = affiliate.affiliateId.ToString();
            }

            int maxrows = 1000;
            bool excel = false;
            if(((Button)sender).Text=="EXCEL")
            {
                excel = true;
                maxrows = 0;
            }
            string sDateFrom = null;
            string sDateTo = null;

            if(dateRange.SelectedValue!="0")
            {
                string[] sDates = dateRange.SelectedValue.Split('~');
                sDateFrom = sDates[0];
                sDateTo = sDates[1];
            }
            else
            {
                sDateFrom = datefrom.Text;
                sDateTo = dateto.Text;            
            }
            string offerIds = null;

            for(int i=0;i<offersDropDown.Items.Count;i++)
            {
                if(offersDropDown.Items[i].Selected)
                {
                    if (offerIds != null)
                        offerIds = offerIds + "," + offersDropDown.Items[i].Value;
                    else
                        offerIds = offersDropDown.Items[i].Value;

                }
            }
            if (offerIds == null) offerIds = "0";

            List<string> fieldNames = new List<string>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(spName, con))
                {
                    SqlDataReader reader;
                    cmd.Parameters.AddWithValue("@datefrom", sDateFrom + " 00:00:00");
                    cmd.Parameters.AddWithValue("@dateto", sDateTo + " 23:59:59");
                    cmd.Parameters.AddWithValue("@advertiserid", selectedAdvertisers);
                    cmd.Parameters.AddWithValue("@affiliate", selectedAffiliates);
                    cmd.Parameters.AddWithValue("@gadvertiser", groupadvertiser.Checked);
                    cmd.Parameters.AddWithValue("@gaffiliate", groupaffiliate.Checked);
                    cmd.Parameters.AddWithValue("@gtag", grouptag.Checked);
                    cmd.Parameters.AddWithValue("@maxrows", maxrows);
                    cmd.Parameters.AddWithValue("@format", groupdate.SelectedValue);
                    cmd.Parameters.AddWithValue("@adminReport", (advertiser==null)?1:0);
                    cmd.Parameters.AddWithValue("@reportType", reportType.SelectedValue);
                    cmd.Parameters.AddWithValue("@offerId", offerIds);
                    cmd.Parameters.AddWithValue("@gOffer", groupOffers.Checked);
                    if (!string.IsNullOrEmpty(selectedCountries))
                    {
                        cmd.Parameters.AddWithValue("@countries", selectedCountries);
                    }
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    //Response.Write(cmd.CommandText);

                    reader =cmd.ExecuteReader();
                    for(int i=0;i<reader.FieldCount;i++)
                    {
                        fieldNames.Add(reader.GetName(i));
                    }

                    daterangeTitle.Text = "Report for : " + sDateFrom + " - " + sDateTo;

                    Int64 clicks = 0;
                    Int64 signups = 0;
                    Int64 conversions = 0;
                    Int64 deposits = 0;

                    Int64 signups4date = 0;
                    Int64 conversions4date = 0;
                    Int64 deposits4date = 0;

                    StringBuilder sb = new StringBuilder();
                    if (!excel)
                    {
                        sb.Append("<thead><tr>");
                        for (int i = 0; i < fieldNames.Count; i++)
                        {
                            sb.Append("<th style=\"width:150px;\">");
                            sb.Append(fieldNames[i]);
                            sb.Append("</th>");
                        }
                        sb.Append("</tr></thead>");
                        sb.Append("<tbody>");
                        while (reader.Read())
                        {
                            sb.Append("<tr>");
                            for (int i = 0; i < fieldNames.Count; i++)
                            {
                                sb.Append("<td>");
                                switch (fieldNames[i])
                                {
                                    case "date":
                                        sb.Append(reader[fieldNames[i]].ToString().Split(' ')[0]);
                                        break;
                                    case "Hour":
                                        sb.Append(reader[fieldNames[i]].ToString() + ":00");
                                        break;
                                    case "month":
                                        string[] date = reader[fieldNames[i]].ToString().Split('/');
                                        sb.Append(date[0] + "/" + date[2].Split(' ')[0]);
                                       break;
                                    default:
                                        sb.Append(reader[fieldNames[i]].ToString());

                                        break;

                                }                                    
                                sb.Append("</td>");
                            }
                            sb.Append("</tr>");
                           
                            if (reader["clicks"] != DBNull.Value)
                            {
                                clicks = clicks + Convert.ToInt64(reader["clicks"]);
                            }

                            if (reportType.SelectedValue == "0" && dateRange.SelectedIndex != 0)
                            {
                                if (reader["signup for date"] != DBNull.Value)
                                {
                                    signups4date += Convert.ToInt64(reader["signup for date"]);
                                }
                                if (reader["signup for date"] != DBNull.Value)
                                {
                                    conversions4date += Convert.ToInt64(reader["conversion for date"]);
                                }
                                if (reader["signup for date"] != DBNull.Value)
                                {
                                    deposits4date += Convert.ToInt64(reader["deposits for date"]);
                                }
                            }
                            else
                            {
                                if (reader["signups"] != DBNull.Value)
                                {
                                    signups = signups + Convert.ToInt64(reader["signups"]);
                                }
                                if (reader["conversions"] != DBNull.Value)
                                {
                                    conversions = conversions + Convert.ToInt64(reader["conversions"]);
                                }
                                if (reader["deposits"] != DBNull.Value)
                                {
                                    deposits = deposits + Convert.ToInt64(reader["deposits"]);
                                }
                            }
                        }
                        sb.Append("</tbody>");
                        sb.Append("<tfoot><tr>");

                            //for (int i = 0; i < fieldNames.Count - 4; i++)
                            //{
                            //    
                            //}
               
                        sb.Append("<td></td><td></td>");

                        sb.Append("<td>");
                        sb.Append(clicks);
                        sb.Append("</td>");
                       
                        if (reportType.SelectedValue == "0" && dateRange.SelectedIndex!=0)
                        {
                            sb.Append("<td>");
                            sb.Append(signups4date);
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(conversions4date);
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(deposits4date);
                            sb.Append("</td>");

                        }
                        else
                        {
                            sb.Append("<td>");
                            sb.Append(signups);
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(conversions);
                            sb.Append("</td>");
                            sb.Append("<td>");
                            sb.Append(deposits);
                            sb.Append("</td>");
                        }

                        sb.Append("</tr>");
                        sb.Append("</tfoot>");
                        LiteralReportData.Text = sb.ToString();
                    }
                    else
                    {
                        for (int i = 0; i < fieldNames.Count; i++)
                        {
                            sb.Append(fieldNames[i]);
                            sb.Append(",");
                        }
                        sb.Append("\n");

                        

                        while (reader.Read())
                        {
                            for (int i = 0; i < fieldNames.Count; i++)
                            {
                                if (fieldNames[i] == "date")
                                    sb.Append(reader[fieldNames[i]].ToString().Split(' ')[0]);
                                else
                                    sb.Append(reader[fieldNames[i]].ToString());
                                sb.Append(",");
                            }
                            sb.Append("\n");
                            clicks = clicks + Convert.ToInt64(reader["clicks"]);
                            if (reportType.SelectedValue == "0" && dateRange.SelectedIndex != 0)
                            {
                                signups4date += Convert.ToInt64(reader["signup for date"]);
                                conversions4date += Convert.ToInt64(reader["conversion for date"]);
                                deposits4date += Convert.ToInt64(reader["deposits for date"]);
                            }
                            else
                            {
                                signups = signups + Convert.ToInt64(reader["signups"]);
                                conversions = conversions + Convert.ToInt64(reader["conversions"]);
                                deposits = deposits + Convert.ToInt64(reader["deposits"]);
                            }

                        }
                        sb.Append("\n");
                        for (int i = 0; i < fieldNames.Count - 4;i++ )
                        {
                            sb.Append(",");
                        }
                        if (reportType.SelectedValue == "0" && dateRange.SelectedIndex != 0)
                        {
                            sb.Append(signups4date);
                            sb.Append(",");
                            sb.Append(conversions4date);
                            sb.Append(",");
                            sb.Append(deposits4date);
                        }
                        else
                        {
                            sb.Append(clicks);
                            sb.Append(",");
                            sb.Append(signups);
                            sb.Append(",");
                            sb.Append(conversions);
                        }
                        sb.Append(",");
                        sb.Append(deposits);
                        ExportToExcel(sb.ToString());
                    }

                   

                   
                    sb.Clear();
                    sb = null;

                    reader.Close();
                    reader = null;
                }
            }
            
            reportPanel.Visible = true;

        }

        public void ExportToExcel(string excel)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment;filename=report.csv");
            Response.Charset = "";
            this.EnableViewState = false;

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

            Response.Write(excel);
            Response.End();
        }

        protected void advertisersDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {

         //   if (ViewState["currentAdvertiser"] == null || (ViewState["currentAdvertiser"] != null && ViewState["currentAdvertiser"].ToString() != advertisersDropDown.SelectedValue && advertisersDropDown.SelectedValue != null))
         //   {
                string advertisersIds = null;
                for (int i = 0; i < advertisersDropDown.Items.Count; i++)
                {
                    if (advertisersDropDown.Items[i].Selected)
                    {
                        if (advertisersIds != null)
                            advertisersIds = advertisersIds + "," + advertisersDropDown.Items[i].Value;
                        else
                            advertisersIds = advertisersDropDown.Items[i].Value;
                    }
                }
                if (advertisersIds == null) advertisersIds = "0";
                Response.Write(advertisersIds);
                ListItem plzSelect = new ListItem("Please select", "0");
                List<offers> myoffers = new List<offers>();
                myoffers = offers.getOfferByAdvertiser(advertisersIds);
                offersDropDown.DataSource = myoffers;
                offersDropDown.DataTextField = "offername";
                offersDropDown.DataValueField = "offerId";
                offersDropDown.DataBind();
                offersDropDown.Items.Insert(0, plzSelect);
                ViewState["currentAdvertiser"] = advertisersDropDown.SelectedValue;
         //   }
        }
}

    

}