﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Partners_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        adminOnly.Visible = false;
        adminOnly3.Visible = false;

		var baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);
		Label1.Text = baseUrl;
		Label2.Text = baseUrl;
		Label3.Text = baseUrl;

		if (Session["affiliate"] == null && Session["advertiser"] == null)
            Response.Redirect("/");
        else if(Session["affiliate"]!=null)
        {
            Affiliates aff = (Affiliates)Session["affiliate"];
            welcomemessage.Text = aff.affiliateName;

            if(aff.affiliateType==1)
            {
                adminOnly.Visible = true;
                adminOnly3.Visible = true;
            }
        }
        else if(Session["advertiser"]!=null)
        {
            advertisers adv = (advertisers)Session["advertiser"];
            welcomemessage.Text = adv.name;
        }

    }
}
