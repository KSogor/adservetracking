﻿<%@ Page MasterPageFile="~/Partners/MasterPage.master" Language="C#" ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeFile="offers.aspx.cs" Inherits="PageMethods.Partners_offers" %>



<asp:Content ID="contentPage" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true" />

    <div id="screenBlocker"></div>




    <asp:Panel ID="offersPanel" Visible="false" runat="server">
        <asp:Repeater runat="server" ID="offerRepeater">

            <HeaderTemplate>
                <table class="affiliatesGrid">
                    <tr>
                        <th>offer.ID</th>
                        <th>advertiserID</th>
                        <th>Offer name</th>
                        <th>Landing Page</th>
                        <th>Category</th>
                        <%if (utils.isManager())
                            { %>
                        <th>Rev.Type</th>
                        <th>Rev.Value</th>
                        <th>Payout.Type</th>
                        <th>Payout.Value</th>
                        <th>Active?</th>
                        <th>Static Param</th>

                        <th>Edit</th>
                        <%} %>
                    </tr>
            </HeaderTemplate>

            <ItemTemplate>
                <tr>
                    <td><%# Eval("offerID") %></td>
                    <td>
                        <asp:Literal runat="server" ID="Literal3"></asp:Literal><%# Eval("advertisername") %></td>
                    <td><%# Eval("offername") %></td>
                    <td><%# Eval("landingPage") %></td>
                    <td>
                        <asp:Literal runat="server" ID="catName"></asp:Literal></td>
                    <%if (utils.isManager())
                        { %>
                    <td>
                        <asp:Literal runat="server" ID="revType"></asp:Literal></td>
                    <td><%# Eval("revenuevalue") %></td>
                    <td>
                        <asp:Literal runat="server" ID="payoutType"></asp:Literal></td>
                    <td><%# Eval("payoutvalue") %></td>
                    <td><%# Eval("active") %></td>
                    <td><%# Eval("staticvalues") %></td>
                    <td><span onclick="editoffer(<%# Eval("offerID") %>);" style="cursor: pointer;">Edit</span></td>
                    <%} %>
                </tr>
            </ItemTemplate>

            <FooterTemplate>
                </table>

            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>

    <div class="addEditForm" id="addEditForm">
        <div class="closeWindow" onclick="hidePopup()">X</div>
        <div class="form-horizontal">
            <div class="form-group">
                <label for="offername" class="col-sm-2 control-label">Offer name</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="offername"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="offerAdvertiser" class="col-sm-2 control-label">Advertiser</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offerAdvertiser">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="landingPage" class="col-sm-2 control-label">Landing Page</label>
                <div class="col-sm-10">
                    <asp:TextBox TextMode="MultiLine" runat="server" class="form-control" ID="landingPage"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="offerCategory" class="col-sm-2 control-label">Category</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offerCategory">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="offerRevenueType" class="col-sm-2 control-label">Revenue Type</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offerRevenueType">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="offerRevenueValue" class="col-sm-2 control-label">Revenue Value</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="offerRevenueValue"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="offerPayoutType" class="col-sm-2 control-label">Payout Type</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offerPayoutType">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="offerPayoutValue" class="col-sm-2 control-label">Payout Value</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="offerPayoutValue"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label for="offerActive" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-10">
                    <asp:DropDownList runat="server" class="form-control" ID="offerActive">
                        <asp:ListItem Value="True">True</asp:ListItem>
                        <asp:ListItem Value="False">False</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="staticvalues" class="col-sm-2 control-label">Static Value</label>
                <div class="col-sm-10">
                    <asp:TextBox runat="server" class="form-control" ID="staticvalues"></asp:TextBox>
                </div>
            </div>
            <asp:HiddenField ID="offerID" runat="server" Value="0" />
            <div>
                <asp:Button runat="server" ID="addoffer" class="button" OnClick="addoffer_Click" Text="Add Offer" value="Add Offer" />
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="managerPanel" Visible="false">
        <div class="managementPanel">
            <div onclick="clearForm()">Add offer</div>



        </div>

        <script type="text/javascript">
            function editoffer(advID) {
                PageMethods.getAdv(advID, Success, Failure);
                return false;
            }

            function Success(data) {
                var jsonObj = eval("(" + data + ")");
                $("#ContentPlaceHolder1_offerID").val(jsonObj.offerID);
                $("#ContentPlaceHolder1_offerAdvertiser").val(jsonObj.advertiserID);
                $("#ContentPlaceHolder1_landingPage").val(jsonObj.lp);
                $("#ContentPlaceHolder1_offerCategory").val(jsonObj.category);
                $("#ContentPlaceHolder1_offerRevenueType").val(jsonObj.revenuetype);
                $("#ContentPlaceHolder1_offerRevenueValue").val(jsonObj.revenuevalue);
                $("#ContentPlaceHolder1_offerPayoutType").val(jsonObj.payouttype);
                $("#ContentPlaceHolder1_offerPayoutValue").val(jsonObj.payoutvalue);
                $("#ContentPlaceHolder1_staticvalues").val(jsonObj.staticvalues);
                $("#ContentPlaceHolder1_offerActive").val(jsonObj.active);
                $("#ContentPlaceHolder1_offername").val(jsonObj.offername);
                $("#ContentPlaceHolder1_addoffer").val("Update Offer");
                showForm();
            }

            function Failure(data) {
                alert("Error getting affiliate data");
            }

            function clearForm() {
                $("#ContentPlaceHolder1_offerID").val(0);
                $("#ContentPlaceHolder1_offerAdvertiser").val("");
                $("#ContentPlaceHolder1_landingPage").val("");
                $("#ContentPlaceHolder1_offerCategory").val("");
                $("#ContentPlaceHolder1_offerRevenueType").val("");
                $("#ContentPlaceHolder1_offerRevenueValue").val("");
                $("#ContentPlaceHolder1_offerPayoutType").val("");
                $("#ContentPlaceHolder1_offerPayoutValue").val("");
                $("#ContentPlaceHolder1_staticvalues").val("");
                $("#ContentPlaceHolder1_offerActive").val("");
                $("#ContentPlaceHolder1_offername").val("");
                $("#ContentPlaceHolder1_addoffer").val("Add new Offer");
                showForm();
            }




            function showForm() {
                document.getElementById("addEditForm").style.display = 'block';
                document.getElementById("screenBlocker").style.display = 'block';
            }
            function hidePopup() {
                document.getElementById("addEditForm").style.display = 'none';
                document.getElementById("screenBlocker").style.display = 'none';
            }
        </script>


    </asp:Panel>


</asp:Content>
