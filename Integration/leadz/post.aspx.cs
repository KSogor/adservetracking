﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Integration_leadz_post : System.Web.UI.Page
{
    public string getFromQuery(string key)
    {
        if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    //http://aditor.com/integration/leadz/post.aspx?lp=67&email=ntestb@test.com&fullName=Alfred%20Einstein&country=israel&gender=M&phone=1321321312
    protected void Page_Load(object sender, EventArgs e)
    {
        string apiKey = HttpContext.Current.Server.UrlEncode("jei%39dEsk25P9mc@8&6jd47Edi");
        string partnerId = HttpContext.Current.Server.UrlEncode("275678");

        string formId = HttpContext.Current.Server.UrlEncode(getFromQuery("lp"));
        string leadCreated = DateTime.Now.ToString();
        string userIp = Request.ServerVariables["REMOTE_ADDR"];
        string email = getFromQuery("email");
        string fullName = getFromQuery("fullName");
        string country = getFromQuery("country");

        string gender = getFromQuery("gender");
        string phone = getFromQuery("phone");
        string birthDate_day = getFromQuery("bd");
        string birthDate_month = getFromQuery("bm");
        string birthDate_year = getFromQuery("by");
        string street = getFromQuery("street");
        string city = getFromQuery("city");
        string state = getFromQuery("state");
        string postalCode = getFromQuery("postalCode");


        using (var client = new WebClient())
        {
            NameValueCollection values = new NameValueCollection();
            values["userIp"] = userIp;
            values["email"] = email;
            values["partnerId"] = partnerId;
            values["apiKey"] = apiKey;
            values["form"] = formId;
            values["fullName"] = fullName.ToLower();
            values["country"] = country;
            values["gender"] = gender;
            values["leadCreated"] = leadCreated;
            values["phone"] = phone;
            

            byte[] response = client.UploadValues("https://leadz.com/api.php?api=SubmitLead", values);
            string responseString = Encoding.Default.GetString(response);

            for(int i=0;i<values.Count;i++)
            {
                Response.Write(values[i].ToString() + "<br/>");
            }
            Response.Write(responseString);
        }
        
    }



}