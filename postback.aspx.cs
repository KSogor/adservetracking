﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class postback : System.Web.UI.Page
{
    private string getFromQuery(string key)
    {

        if (Request.QueryString[key] != null)
            return HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString());
        else
            return "";
    }
    private int getIntFromQuery(string key)
    {

		if (!string.IsNullOrEmpty(Request.QueryString[key]))
			return Convert.ToInt32(HttpContext.Current.Server.UrlEncode(Request.QueryString[key].ToString()));
        else
            return 0;
    }
	private double? getDecimalFromQuery(string key)
	{
		if (!string.IsNullOrEmpty(Request.QueryString[key]))
		{
			var paramValue = Request.QueryString[key].ToString();
			return double.Parse(paramValue);
		}
		else
			return null;
	}

	private bool? getBoolFromQuery(string key)
	{
		if (!string.IsNullOrEmpty(Request.QueryString[key]))
		{
			var paramValue = Request.QueryString[key].ToString();
			if (paramValue.Length == 1) { if (paramValue == "1") return true; else return false; }
			return Convert.ToBoolean(paramValue);
		}
		else
			return null;
	}
	protected void Page_Load(object sender, EventArgs e)
    {
        string clickId = getFromQuery("clickid");
        bool isCookie=false;
        if(string.IsNullOrWhiteSpace(clickId))
        {
            if (Request.Cookies["aditorcookie"] != null)
            {
                if (Request.Cookies["aditorcookie"]["clickid"] != null)
                { 
                    clickId = Request.Cookies["aditorcookie"]["clickid"];
                    isCookie = true;
                }
            }
        }
        int conversionType = getIntFromQuery("conversion");
		string googleaid = getFromQuery("googleaid");
		string iosifa = getFromQuery("iosifa");
		double? deposit_amount = getDecimalFromQuery("deposit_amount");
		bool? fd = getBoolFromQuery("fd");
		//updating conversion
        conversions.updateConversions(clickId, conversionType, googleaid, iosifa, deposit_amount, fd, Request.Url.OriginalString);
		
        //sending postback to affiliate 
        if(!string.IsNullOrWhiteSpace(clickId)) postbackCS.postbackSend(clickId,conversionType);    

        Response.Write("click : " + clickId + "<br/>was registered succesfully via " + ((isCookie)?"cookie":"S2S"));
    }
}


        